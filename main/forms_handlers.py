from django.core.urlresolvers import reverse
from django.shortcuts import redirect
from django.utils.translation import gettext as _

from main.forms import EmailRecipientsForm
from main.models import EmailRecipients, Person, Product


def handle_error(request, title, message, object_with_error):
    error = {}
    error['title'] = title
    error['message'] = message
    error['object'] = object_with_error
    request.session['error'] = error

    return None


def handle_success(request, title, message, object_with_success):
    success = {}
    success['title'] = title
    success['message'] = message
    success['object'] = object_with_success
    request.session['success'] = success

    return None


def handle_email_recipients(request, store):
    if 'add_email_recipient' in request.POST:
        email_cc_form = EmailRecipientsForm(request.POST)

        if email_cc_form.is_valid():
            if email_cc_form.cleaned_data['main']:
                for cc in store.get_cc():
                    cc.main = False
                    cc.save()

            email_recipient = email_cc_form.save(commit=False)
            email_recipient.store = store
            email_recipient.save()

            success = {}
            success['title'] = _('Recipiente de email criado com sucesso!')
            success['message'] = _('Novo recipiente de email:')
            success['object'] = email_cc_form.cleaned_data['email']
            request.session['success'] = success

            return redirect(reverse('configuracoes'))

    elif 'delete_email' in request.POST:
        email_recipient = EmailRecipients.objects.get(id=request.POST['delete_email'])
        email = email_recipient.email
        email_recipient.delete()

        success = {}
        success['title'] = _('Recipiente de email deletado com sucesso!')
        success['message'] = _('Recipiente deletado:')
        success['object'] = email
        request.session['success'] = success

        return redirect(reverse('configuracoes'))

    elif 'change_main_email' in request.POST:
        email_recipient = EmailRecipients.objects.get(id=request.POST['change_main_email'])
        email = email_recipient.email

        if email_recipient.main is True:
            email_recipient.main = False
            email_recipient.save()

            success = {}
            success['title'] = _('Email principal retirado!')
            success['message'] = _('Email:')
            success['object'] = email
            request.session['success'] = success

        else:
            for er in store.get_cc():
                if er.main is True:
                    er.main = False
                    er.save()

            email_recipient.main = True
            email_recipient.save()

            title = _('Email principal alterado!')
            message = _('Email:')
            object_with_success = email
            handle_success(request, title, message, object_with_success)

        return redirect(reverse('configuracoes'))

    title = _('Erro ao adicionar recipiente de email.')
    message = _('Verifique os campos e tente novamente.')
    object_with_error = ''
    handle_error(request, title, message, object_with_error)

    return redirect(reverse('configuracoes'))


def handle_services(request):
    if 'change_service_required' in request.POST:
        service = Product.objects.get(id=request.POST['change_service_required'])

        service.required = not service.required
        service.save()

        if service.required:
            title = _('Serviço atualizado para obrigatório.')
        else:
            title = _('Serviço atualizado para não obrigatório.')
        message = _('Serviço:')
        object_with_success = service.name
        handle_success(request, title, message, object_with_success)

        return redirect(reverse('servicos'))

    elif 'change_service_active' in request.POST:
        service = Product.objects.get(id=request.POST['change_service_active'])

        service.active = not service.active
        service.save()

        if service.active:
            title = _('Serviço ativado.')
        else:
            title = _('Serviço inativado.')
        message = _('Serviço:')
        object_with_success = service.name
        handle_success(request, title, message, object_with_success)

        return redirect(reverse('servicos'))

    title = _('Erro ao alterar serviço.')
    message = _('Verifique os campos e tente novamente.')
    object_with_error = service.name
    handle_error(request, title, message, object_with_error)


def handle_update_form(request,
                       model,
                       model_form,
                       feedback_success={
                           'title': _('Dados atualizados com sucesso!'),
                           'message': _(''),
                           'object_with_success': ''
                       },
                       feedback_error={
                           'title': 'Erro! Verifique se os campos foram preenchidos corretamente.',
                           'message': '',
                           'object_with_error': ''
                       }):
    form = model_form(instance=model, data=request.POST)
    fields_updated = {}
    fields_with_error = {}

    if form.is_valid():
        fields = model.get_fields()

        for field in fields:
            if form.has_changed():
                for field in form.changed_data:
                    fields_updated[field] = _('Campo atualizado com sucesso.')
                model.save()

        request.session['fields_updated'] = fields_updated

        if len(fields_updated) > 0:
            title = feedback_success['title']
            message = feedback_success['message']
            object_with_success = feedback_success['object_with_success']
            handle_success(request, title, message, object_with_success)

        return redirect(reverse('configuracoes'))

    for field, error in form.errors.items():
        fields_with_error[field] = error

    request.session['fields_with_error'] = fields_with_error

    title = feedback_error['title']
    message = feedback_error['message']
    object_with_error = feedback_error['object_with_error']

    handle_error(request, title, message, object_with_error)

    return redirect(reverse('configuracoes'))


def handle_users(request):
    if 'change_person_admin' in request.POST:
        person = Person.objects.get(id=request.POST['change_person_admin'])

        person.admin = not person.admin
        person.save()

        if person.admin:
            title = _('Usuário alterado para admin.')
        else:
            title = _('Usuário alterado para não admin.')
        message = _('Usuário:')
        object_with_success = person.user.username
        handle_success(request, title, message, object_with_success)

        return redirect(reverse('usuarios'))

    elif 'change_person_active' in request.POST:
        person = Person.objects.get(id=request.POST['change_person_active'])

        person.user.is_active = not person.user.is_active
        person.user.save()

        if person.user.is_active:
            title = _('Ativado usuário.')
        else:
            title = _('Inativado usuário.')
        message = _('Usuário:')
        object_with_success = person.user.username
        handle_success(request, title, message, object_with_success)

        return redirect(reverse('usuarios'))

    title = _('Erro ao alterar usuário.')
    message = _('Verifique os campos e tente novamente.')
    object_with_error = person.user.username
    handle_error(request, title, message, object_with_error)
