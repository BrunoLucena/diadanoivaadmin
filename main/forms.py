# -*- coding: utf-8 -*-
from django import forms
from django.contrib.auth import (
    authenticate, get_user_model,
)
from django.forms import ModelForm
from django.utils.text import capfirst
from django.utils.translation import ugettext_lazy as _

from main.models import *


class AuthenticationFormCustom(forms.Form):
    """
    Base class for authenticating users. Extend this to get a form that accepts
    username/password logins.
    """
    username = forms.CharField(max_length=254)
    password = forms.CharField(label=_("Password"), widget=forms.PasswordInput)

    error_messages = {
        'invalid_login': _("Usuário ou senha inválidos."
                           "Ambos os campos podem diferenciar"
                           " maiúsculas e minúsculas."),
        'inactive': _("Esta conta está inativa."),
    }

    def __init__(self, request=None, *args, **kwargs):
        """
        The 'request' parameter is set for custom auth use by subclasses.
        The form data comes in via the standard 'data' kwarg.
        """
        self.request = request
        self.user_cache = None
        super(AuthenticationFormCustom, self).__init__(*args, **kwargs)
        for field in iter(self.fields):
            self.fields[field].widget.attrs.update(
                {
                    'class': 'form-control'
                }
            )

        # Set the label for the "username" field.
        UserModel = get_user_model()
        self.username_field = UserModel._meta.get_field(
            UserModel.USERNAME_FIELD
        )
        if self.fields['username'].label is None:
            self.fields['username'].label = capfirst(
                self.username_field.verbose_name
            )

    def clean(self):
        username = self.cleaned_data.get('username')
        password = self.cleaned_data.get('password')

        if username and password:
            self.user_cache = authenticate(username=username,
                                           password=password)
            if self.user_cache is None:
                raise forms.ValidationError(
                    self.error_messages['invalid_login'],
                    code='invalid_login',
                    params={'username': self.username_field.verbose_name},
                )
            else:
                self.confirm_login_allowed(self.user_cache)

        return self.cleaned_data

    def confirm_login_allowed(self, user):
        """
        Controls whether the given User may log in. This is a policy setting,
        independent of end-user authentication. This default behavior is to
        allow login by active users, and reject login by inactive users.

        If the given user cannot log in, this method should raise a
        ``forms.ValidationError``.

        If the given user may log in, this method should return None.
        """
        if not user.is_active:
            raise forms.ValidationError(
                self.error_messages['inactive'],
                code='inactive',
            )

    def get_user_id(self):
        if self.user_cache:
            return self.user_cache.id
        return None

    def get_user(self):
        return self.user_cache


class ContactForm(forms.Form):
    subject = forms.CharField(
        max_length=1000,
        label='Assunto'
    )
    message = forms.CharField(
        max_length=9999,
        label='Mensagem',
        widget=forms.Textarea(
            attrs={
                'class': 'contact_field'
            }
        )
    )
    email = forms.EmailField(max_length=1000, label='Email')


class EmailRecipientsForm(ModelForm):
    class Meta:
        model = EmailRecipients
        exclude = ['store']

    def __init__(self, *args, **kwargs):
        super(EmailRecipientsForm, self).__init__(*args, **kwargs)
        for field in iter(self.fields):
            if field != 'main':
                self.fields[field].widget.attrs.update(
                    {
                        'class': 'form-control',
                        'placeholder': self.fields[field].label,
                    }
                )


class OrderHistoricModelForm(ModelForm):
    class Meta:
        model = OrderHistoric
        exclude = ['order', 'date', 'proposal']

    def __init__(self, *args, **kwargs):
        super(OrderHistoricModelForm, self).__init__(*args, **kwargs)
        for field in iter(self.fields):
            self.fields[field].widget.attrs.update(
                {
                    'class': 'form-control',
                    'placeholder': self.fields[field].label,
                }
            )


class OrderInstallmentsModelForm(ModelForm):
    class Meta:
        model = OrderInstallments
        exclude = ['order', 'payment', 'paymentDate',
                   ]


class OrderModelForm(ModelForm):
    class Meta:
        model = Order
        exclude = ['order_number', 'date_created', 'date_closed',
                   'status', 'total', 'total2', 'discount', 'paymentType',
                   'installments']

    def __init__(self, *args, **kwargs):
        super(OrderModelForm, self).__init__(*args, **kwargs)
        for field in iter(self.fields):
            self.fields[field].widget.attrs.update(
                {
                    'class': 'form-control',
                    'placeholder': self.fields[field].label,
                }
            )


class OrderModelForm2(ModelForm):
    class Meta:
        model = Order
        exclude = ['order_number', 'store', 'date_wedding', 'date_test',
                   'time_test', 'time_arrival', 'time_leaving', 'email',
                   'date_created', 'date_closed', 'payment_type',
                   'status', 'total', 'total2', 'discount', 'paymentType',
                   'installments', 'date_return']

    def __init__(self, *args, **kwargs):
        super(OrderModelForm2, self).__init__(*args, **kwargs)
        for field in iter(self.fields):
            self.fields[field].widget.attrs.update(
                {
                    'class': 'form-control',
                    'placeholder': self.fields[field].label,
                }
            )


class OrderUpdateModelForm(ModelForm):
    class Meta:
        model = Order
        exclude = ['order_number', 'store', 'date_created', 'date_closed',
                   'total', 'total2', 'discount', 'payment_type',
                   'installments', 'time_arrival', 'time_leaving',
                   'address', 'city', 'state', 'cep', 'cpf', 'rg',
                   'professional_hair', 'professional_make_up', 'date_test',
                   'time_test', 'number', 'complement', 'date_wedding',
                   'date_return', 'district']

    def __init__(self, *args, **kwargs):
        super(OrderUpdateModelForm, self).__init__(*args, **kwargs)
        for field in iter(self.fields):
            self.fields[field].widget.attrs.update(
                {
                    'class': 'form-control',
                    'placeholder': self.fields[field].label,
                }
            )


class OrderQuickModelForm(ModelForm):
    class Meta:
        model = Order
        exclude = ['order_number', 'store', 'time_leaving', 'status',
                   'total', 'addess', 'number', 'complement',
                   'district', 'city', 'state', 'cep', 'cpf', 'rg',
                   'date_created', 'date_return', 'date_closed',
                   'discount', 'total2', 'time_arrival', 'address',
                   'professional_hair', 'professional_make_up', 'date_test',
                   'time_test', 'payment_type', 'installments',
                   'date_wedding'
                   ]

    def __init__(self, *args, **kwargs):
        super(OrderQuickModelForm, self).__init__(*args, **kwargs)
        for field in iter(self.fields):
            self.fields[field].widget.attrs.update(
                {
                    'class': 'form-control',
                    'placeholder': self.fields[field].label,
                }
            )


class PaymentMethodsModelForm(ModelForm):
    class Meta:
        model = PaymentMethods
        exclude = []

    def __init__(self, *args, **kwargs):
        super(PaymentMethodsModelForm, self).__init__(*args, **kwargs)
        for field in iter(self.fields):
            self.fields[field].widget.attrs.update(
                {
                    'class': 'form-control',
                    'placeholder': self.fields[field].label,
                }
            )


class PersonMaxDiscountModelForm(ModelForm):
    class Meta:
        model = Person
        fields = ['max_discount']

    def __init__(self, *args, **kwargs):
        super(PersonMaxDiscountModelForm, self).__init__(*args, **kwargs)
        for field in iter(self.fields):
            self.fields[field].widget.attrs.update(
                {
                    'class': 'form-control',
                    'placeholder': self.fields[field].label,
                }
            )


class PersonModelForm(ModelForm):
    class Meta:
        model = Person
        exclude = ['user', 'balance', 'employee', 'employer', 'admin',
                   'cpf', 'address', 'number', 'complement', 'district',
                   'ddd', 'phone', 'city', 'state', 'cep']


class ProductPriceModelForm(ModelForm):
    class Meta:
        model = ProductPrice
        fields = ['price', 'date_start', 'date_end']

    def __init__(self, *args, **kwargs):
        super(ProductPriceModelForm, self).__init__(*args, **kwargs)
        for field in iter(self.fields):
            if field != 'required':
                self.fields[field].required = True
                self.fields[field].widget.attrs.update(
                    {
                        'class': 'form-control',
                        'placeholder': self.fields[field].label,
                    }
                )


class ProductUpdateModelForm(ModelForm):
    class Meta:
        model = Product
        fields = ['id', 'name']
        widgets = {
            'description': forms.Textarea(
                attrs={
                    'rows': 10,
                    'cols': 52,
                    'style': 'resize: none;'
                }
            ),
        }


class ServiceModelForm(ModelForm):
    class Meta:
        model = Product
        fields = ['name', 'default_price', 'required', 'active']

    def __init__(self, *args, **kwargs):
        super(ServiceModelForm, self).__init__(*args, **kwargs)
        for field in iter(self.fields):
            if field != 'active' and field != 'required':
                self.fields[field].required = True
                self.fields[field].widget.attrs.update(
                    {
                        'class': 'form-control',
                        'placeholder': self.fields[field].label,
                    }
                )


class StoreModelForm(ModelForm):
    class Meta:
        model = Store
        exclude = ['logo', 'short_name', 'owner']

    def __init__(self, *args, **kwargs):
        super(StoreModelForm, self).__init__(*args, **kwargs)
        for field in iter(self.fields):
            if field != 'logo':
                self.fields[field].required = True
                self.fields[field].widget.attrs.update(
                    {
                        'class': 'form-control',
                        'placeholder': self.fields[field].label,
                    }
                )


class UserIsActiveModelForm(ModelForm):
    class Meta:
        model = User
        fields = ['is_active']

    def render_option(
        self,
        selected_choices,
        option_value,
        option_label,
        option_title=''
    ):
        option_value = forms.util.force_unicode(option_value)
        if option_value in selected_choices:
            selected_html = u' selected="selected"'
            if not self.allow_multiple_selected:
                # Only allow for a single selection.
                selected_choices.remove(option_value)
        else:
            selected_html = ''
        return u'<option title="%s" value="%s"%s>%s</option>' % (
            escape(option_title),
            escape(option_value),
            selected_html,
            forms.util.conditional_escape(
                forms.util.force_unicode(option_label)
            )
        )


class UserIsAdminModelForm(ModelForm):
    class Meta:
        model = Person
        fields = ['admin']

    def render_option(
        self,
        selected_choices,
        option_value,
        option_label,
        option_title=''
    ):
        option_value = forms.util.force_unicode(option_value)
        if option_value in selected_choices:
            selected_html = u' selected="selected"'
            if not self.allow_multiple_selected:
                # Only allow for a single selection.
                selected_choices.remove(option_value)
        else:
            selected_html = ''
        return u'<option title="%s" value="%s"%s>%s</option>' % (
            escape(option_title),
            escape(option_value),
            selected_html,
            forms.util.conditional_escape(
                forms.util.force_unicode(option_label)
            )
        )


class UserModelForm(ModelForm):
    class Meta:
        model = User
        fields = ['username', 'first_name', 'last_name',
                  'email', 'is_superuser']

    def __init__(self, *args, **kwargs):
        super(UserModelForm, self).__init__(*args, **kwargs)
        for field in iter(self.fields):
            if field != 'is_superuser':
                self.fields[field].required = True
                self.fields[field].widget.attrs.update(
                    {
                        'class': 'form-control',
                        'placeholder': self.fields[field].label,
                    }
                )
            else:
                self.fields[field].label = 'Admin'
                self.fields[field].widget.attrs.update(
                    {
                        'class': 'input-align',
                    }
                )


class UserModelForm2(ModelForm):
    class Meta:
        model = User
        fields = ['username', 'first_name', 'last_name', 'email']

    def __init__(self, *args, **kwargs):
        super(UserModelForm2, self).__init__(*args, **kwargs)
        for field in iter(self.fields):
            if field != 'is_superuser':
                self.fields[field].widget.attrs.update(
                    {
                        'class': 'form-control',
                        'placeholder': self.fields[field].label,
                    }
                )


class UserPassword(ModelForm):
    class Meta:
        model = User
        fields = ['password']

    def __init__(self, *args, **kwargs):
        super(UserPassword, self).__init__(*args, **kwargs)
        for field in iter(self.fields):
            self.fields['password'].widget = forms.PasswordInput(attrs={})
            self.fields[field].widget.attrs.update(
                {
                    'class': 'form-control',
                    'placeholder': self.fields[field].label,
                }
            )
