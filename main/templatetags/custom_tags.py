from django import template
from django.utils import safestring

register = template.Library()


@register.filter
def addstr(arg1, arg2):
    """Concatenate arg1 & arg2"""
    return str(arg1) + str(arg2)


@register.filter(name='ng')
def Angularify(value):
    return safestring.mark_safe('{{%s}}' % value)


@register.filter(name='percentage_minus')
def percentage_minus(value, percentage):
    value = int(value)
    percentage = int(percentage)
    return round((value - value * percentage / 100), 2)


@register.filter(name='percentage_plus')
def percentage_plus(value, percentage):
    value = int(value)
    percentage = int(percentage)
    return round((value + value * percentage / 100), 2)


@register.filter(name='real')
def ponto_para_virgula(a):
    return str(a).replace('.', ',')


@register.filter(name='dolar')
def virgula_para_ponto(a):
    return str(a).replace(',', '.')


@register.filter(name='multiply')
def multiply(value, times):
    a = float(value) * times
    return round(a, 2)


@register.filter(name='sum')
def sum(a, b):
    return a + b
