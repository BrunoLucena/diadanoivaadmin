from django.core.urlresolvers import reverse
from django.shortcuts import redirect

from main.models import Order, Product, Proposal, TokenAccess, User


def check_token_access(function):
    def wrap(request, *args, **kwargs):
        token = TokenAccess.objects.get(token=kwargs['token_access'])

        if token.is_expired() or not token.same_page(request.get_full_path()):
            return redirect(reverse('not_authorized'))
        else:
            return function(request, *args, **kwargs)

    wrap.__doc__ = function.__doc__
    wrap.__name__ = function.__name__

    return wrap


def person_is_admin(function):
    def wrap(request, *args, **kwargs):
        person = request.user.person
        if not person.admin:
            return redirect(reverse('not_authorized'))
        else:
            return function(request, *args, **kwargs)

    wrap.__doc__ = function.__doc__
    wrap.__name__ = function.__name__

    return wrap


def user_store_match_store_order(function):
    def wrap(request, *args, **kwargs):
        order = Order.objects.get(id=kwargs['order_id'])
        store = order.store
        self_store = request.user.person.store
        if store != self_store:
            return redirect(reverse('not_authorized'))
        else:
            return function(request, *args, **kwargs)

    wrap.__doc__ = function.__doc__
    wrap.__name__ = function.__name__

    return wrap


def user_store_match_store_proposal(function):
    def wrap(request, *args, **kwargs):
        proposal = Proposal.objects.get(id=kwargs['proposalId'])
        order = proposal.order
        store = order.store
        self_store = request.user.person.store

        if store != self_store:
            return redirect(reverse('not_authorized'))
        else:
            return function(request, *args, **kwargs)

    wrap.__doc__ = function.__doc__
    wrap.__name__ = function.__name__

    return wrap


def user_store_match_store_service(function):
    def wrap(request, *args, **kwargs):
        service = Product.objects.get(id=kwargs['service_id'])
        store = service.store
        self_store = request.user.person.store
        if store != self_store:
            return redirect(reverse('not_authorized'))
        else:
            return function(request, *args, **kwargs)

    wrap.__doc__ = function.__doc__
    wrap.__name__ = function.__name__

    return wrap


def user_store_match_store_user_edit(function):
    def wrap(request, *args, **kwargs):
        person = request.user.person
        user_edit = User.objects.get(id=kwargs['user_id'])
        person_edit = user_edit.person

        if person_edit not in person.store.get_persons():
            return redirect(reverse('not_authorized'))
        else:
            return function(request, *args, **kwargs)

    wrap.__doc__ = function.__doc__
    wrap.__name__ = function.__name__

    return wrap
