# -*- coding: utf-8 -*-

# Python imports
import datetime
from dateutil.relativedelta import relativedelta
from decimal import *

# Django imports
from django.contrib.auth import login, logout
from django.contrib.auth.views import *
from django.contrib.auth.forms import *  # AuthenticationForm()...
from django.contrib.auth.models import *  # User, ...
from django.contrib.auth.decorators import login_required
from django.core.urlresolvers import reverse
from django.db import IntegrityError
from django.shortcuts import render, redirect, render_to_response
from django.template import RequestContext
from django.utils.dateformat import DateFormat
from django.utils.formats import get_format
from django.utils.timezone import now
from django.utils.translation import gettext as _
# from django.views.decorators.http import require_http_methods

# Local imports
from main.decorators import *
from main.forms import *
from main.forms_handlers import *
from main.functions import *
from main.models import *


# Views
def page_not_found(request):
    response = render_to_response('main/404.html', {},
                                  context_instance=RequestContext(request))
    response.status_code = 404
    return response


def server_error(request):
    response = render_to_response('main/500.html', {},
                                  context_instance=RequestContext(request))
    response.status_code = 500
    return response


@login_required(login_url='/login/')
def home(request):
    user = request.user
    person = user.person
    store = person.store

    orders = Order.objects.all().filter(store=store).order_by('date_return')

    if request.method == "GET":
        if "search" in request.GET:

            search = request.GET["search"]

            results = Order.objects.all().filter(
                store=store,
                name__icontains=search)

            return render(request, 'main/base.html', locals())

    return render(request, 'main/base.html', locals())


def teste(request):
    errors = {}
    if 'loginusertest' in request.POST:
        username = request.POST['user_test']
        email = request.POST['email_test']
        password = request.POST['password_test']
        try:
            user = User.objects.create_user(
                username=username,
                email=email,
                password=password)
            print(user)
            try:
                store = Store.objects.get_or_create(
                    name='Dia da Noiva',
                    owner=User.objects.get(username='Bruno'))[0]
                person = Person.objects.get_or_create(
                    user=user,
                    store=store)[0]
                print(store)
                print(person)
            except Exception as e:
                print(e)
                print(1)
                errors['user'] = False
                return render(request, 'main/login_test.html', locals())
        except Exception as e:
            print(e)
            print(2)
            errors['user'] = False
            return render(request, 'main/login_test.html', locals())

        try:
            logout(request)
            login(request, user)
            return redirect(reverse('login'), args=[user])
        except Exception as e:
            print(e)
            print(3)
            errors['user'] = True
            return render(request, 'main/login_test.html', locals())

    return render(request, 'main/login_test.html', locals())


@login_required(login_url='/loing/')
def not_authorized(request):
    return render(request, 'main/not_authorized.html', locals())


@login_required(login_url='/login/')
def perfil(request):
    return render(request, 'main/perfil.html', locals())


@login_required(login_url='/login/')
@person_is_admin
def servicos(request):
    session = check_session_form(request)
    success, error, fields_updated, fields_with_error = session.values()

    if 'messages' in request.session:
        messages = request.session['messages']
        del request.session['messages']

    store = request.user.person.store
    services = store.get_services()

    if 'change_service_required' in request.POST or 'change_service_active' in request.POST:
        return handle_services(request)

    return render(request, 'main/servicos.html', locals())


@login_required(login_url='/login/')
@person_is_admin
def configuracoes(request):
    session = check_session_form(request)
    success, error, fields_updated, fields_with_error = session.values()

    store = request.user.person.store
    email_recipients = store.get_cc()

    email_cc_form = EmailRecipientsForm
    store_form = StoreModelForm(instance=store)

    if request.method == 'POST':
        if ('add_email_recipient' in request.POST or
                'delete_email' in request.POST or
                'change_main_email' in request.POST):

            return handle_email_recipients(request, store)

        elif 'store_info' in request.POST:
            feedback_success = {
                'title': 'Dados da Loja atualizados com sucesso!',
                'message': '',
                'object_with_success': ''
            }
            feedback_error = {
                'title': 'Erro! Verifique se os campos foram preenchidos corretamente.',
                'message': '',
                'object_with_error': ''
            }

            return handle_update_form(request, store, StoreModelForm, feedback_success, feedback_error)

    return render(request, 'main/configuracoes.html', locals())


@login_required(login_url='/login/')
@person_is_admin
def criar_servico(request):
    user = request.user
    person = user.person
    if not person.admin:
        return redirect(reverse('not_authorized'))

    store = request.user.person.store
    how_many_prices = 1
    service_form = ServiceModelForm()
    user_password = UserPassword()
    person_max_discount_model_form = PersonMaxDiscountModelForm()

    errors = {}

    if request.method == 'POST':
        if "create_service" in request.POST:
            POST = request.POST
            service_form = ServiceModelForm(request.POST)

            prices_index = []
            for field in POST:
                if 'service_price' in str(field):
                    prices_index.append(str(field).replace('service_price_',
                                                           ''))
            how_many_prices = len(prices_index)
            prices = {}

            if service_form.is_valid():
                for index in prices_index:
                    service_price = 'service_price_' + index
                    service_date_start = 'service_date_start_' + index
                    service_date_end = 'service_date_end_' + index
                    if (POST[service_price] == str(0) and
                            POST[service_date_start] == '' and
                            POST[service_date_end] == ''):
                        pass
                    elif(POST[service_price] == '' or
                            POST[service_date_start] == '' or
                            POST[service_date_end] == ''):
                        errors[index] = ('Todos os campos dos preços'
                                         ' devem ser preenchidos')
                    else:
                        try:
                            round(float(POST[service_price]), 2)
                            Order.str_to_date(POST[service_date_start])
                            Order.str_to_date(POST[service_date_start])
                        except Exception:
                            errors[index] = service_price
                        prices[index] = {}
                        prices[index]['price'] = POST[service_price]
                        prices[index]['date_start'] = POST[service_date_start]
                        prices[index]['date_end'] = POST[service_date_end]

                service = service_form.save(commit=False)
                service.store = store
                try:
                    service.save()
                except IntegrityError:
                    errors['unique_together'] = True
                    service_form.add_error('name', _('Já existe um serviço com este nome.'))

                if len(errors) > 0:
                    return render(request, 'main/criar_servico.html', locals())

                for price_all in prices:
                    price = prices[price_all]['price']
                    date_start = prices[price_all]['date_start']
                    date_end = prices[price_all]['date_end']
                    service.add_price(price, date_start, date_end)

                title = _('Serviço criado com sucesso!')
                message = _('Novo serviço:')
                object_with_success = service.name
                handle_success(request, title, message, object_with_success)

                return redirect(reverse('servicos'))

    return render(request, 'main/criar_servico.html', locals())


@login_required(login_url='/login/')
@person_is_admin
@user_store_match_store_service
def editar_servico(request, service_id):
    '''Ver, editar e excluir serviço.
    Ver, criar, editar e excluir preços,
    '''
    user = request.user
    person = user.person

    service = Product.objects.get(id=service_id)
    store = service.store
    store_self = person.store

    if 'updated' in request.session:
        updated = request.session['updated']
        del request.session['updated']
    else:
        updated = []

    service_form = ServiceModelForm(instance=service)

    prices_service = service.get_all_prices()
    how_many_prices = 1

    errors = {}
    updated = {}
    success = {}

    if request.method == 'POST':
        if 'update_service' in request.POST:
            POST = request.POST
            original = {}
            original['name'] = service.name
            original['default_price'] = service.default_price
            original['required'] = service.required
            service_form = ServiceModelForm(request.POST, instance=service)

            # Saves service info
            if service_form.is_valid():
                cleaned_data = service_form.cleaned_data
                if service_form.has_changed():
                    for field in service_form.changed_data:
                        updated[field] = 'Campo atualizado com sucesso'
                    service_form.save()

            # Delete selected prices
            if 'price_delete' in POST:
                delete_prices = POST.getlist('price_delete')
                success['delete_price'] = {}
                for p in delete_prices:
                    p_id = p.replace('id_service_', '')
                    price = service.delete_price(p_id)
                    success['delete_price'][p_id] = ('Preço'
                                                     ' deletado com sucesso')

            # Update prices that already exists
            prices_exists = {}
            for field in POST:
                if 'id_price_' in field:
                    id = field.replace('id_price_', '')
                    price = POST['id_price_' + id]
                    date_start = POST['id_date_start_' + id]
                    date_end = POST['id_date_end_' + id]
                    prices_exists[id] = {}
                    prices_exists[id]['price'] = price
                    prices_exists[id]['date_start'] = date_start
                    prices_exists[id]['date_end'] = date_end

            if len(prices_exists) > 0:
                for price in prices_exists:
                    if ('id_service_' +
                            str(price)) not in POST.getlist('price_delete'):
                        data = {
                            'product': service,
                            'price': round(
                                float(
                                    prices_exists[price]['price']
                                ), 2
                            ),
                            'date_start': Order.str_to_date(
                                prices_exists[price]['date_start']
                            ),
                            'date_end': Order.str_to_date(
                                prices_exists[price]['date_end']
                            )
                        }
                        p = service.get_price(price)
                        p_form = ProductPriceModelForm(data, instance=p)
                        if p_form.has_changed():
                            for field in p_form.changed_data:
                                updated[field] = 'Campo atualizado com sucesso'
                            updated['service_prices'] = True
                            p_form.save()

            # Process new prices
            prices_index = []
            for field in POST:
                if 'service_price' in str(field):
                    prices_index.append(str(field).replace('service_price_',
                                                           ''))
            how_many_prices = len(prices_index)
            prices = {}

            for index in prices_index:
                service_price = 'service_price_' + index
                service_date_start = 'service_date_start_' + index
                service_date_end = 'service_date_end_' + index
                if (POST[service_price] == str(0) and
                        POST[service_date_start] == '' and
                        POST[service_date_end] == ''):
                    pass
                elif (POST[service_price] == '' or
                        POST[service_date_start] == '' or
                        POST[service_date_end] == ''):
                    errors[index] = ('Todos os campos dos'
                                     ' preços devem ser preenchidos')
                else:
                    try:
                        round(float(POST[service_price]), 2)
                        Order.str_to_date(POST[service_date_start])
                        Order.str_to_date(POST[service_date_start])
                    except Exception:
                        errors[index] = service_price
                    prices[index] = {}
                    prices[index]['price'] = POST[service_price]
                    prices[index]['date_start'] = POST[service_date_start]
                    prices[index]['date_end'] = POST[service_date_end]
            if len(errors) > 0:
                return render(request, 'main/editar_servico.html', locals())

            success['insert_prices'] = {}
            for price_all in prices:
                price = prices[price_all]['price']
                date_start = prices[price_all]['date_start']
                date_end = prices[price_all]['date_end']
                service.add_price(price, date_start, date_end)
                success['insert_prices'][price_all] = ('Preço'
                                                       'inserido com sucesso')

            return render(request, 'main/editar_servico.html', locals())

        elif 'delete_service' in request.POST:
            request.session['messages'] = {'delete': service.name}
            service.delete()
            return redirect(reverse('servicos'))

    return render(request, 'main/editar_servico.html', locals())


@login_required(login_url='/login/')
@person_is_admin
def usuarios(request):
    session = check_session_form(request)
    success, error, fields_updated, fields_with_error = session.values()

    if 'messages' in request.session:
        messages = request.session['messages']
        del request.session['messages']

    person = request.user.person
    store = request.user.person.store
    persons = Person.objects.all().filter(store=store).order_by('-user__date_joined')

    if request.method == 'POST':
        if 'change_person_admin' in request.POST or 'change_person_active' in request.POST:
            return handle_users(request)

    return render(request, 'main/usuarios.html', locals())


@login_required(login_url='/login/')
@person_is_admin
def criar_usuario(request):
    person = request.user.person
    store = request.user.person.store
    user_form = UserModelForm()
    user_password = UserPassword()
    person_max_discount_model_form = PersonMaxDiscountModelForm()

    if request.method == 'POST':
        if "create_user" in request.POST:
            user_form = UserModelForm(request.POST)
            user_password = UserPassword(request.POST)
            person_max_discount_model_form = PersonMaxDiscountModelForm(
                request.POST)

            if (user_form.is_valid() and
                    user_password.is_valid() and
                    person_max_discount_model_form.is_valid()):
                user = user_form.save(commit=False)

                username = user_form.cleaned_data['username']
                email = user_form.cleaned_data['email']
                is_superuser = user_form.cleaned_data['is_superuser']
                password = user_password.cleaned_data['password']

                user.username = username
                user.email = email
                user.set_password(password)
                user.is_superuser = False
                user.save()

                person = Person.objects.create(user=user, store=store)
                person.store = store
                person.max_discount = person_max_discount_model_form.cleaned_data['max_discount']
                if is_superuser:
                    person.admin = True
                person.save()

                title = _('Usuário criado com sucesso!')
                message = _('Novo usuário:')
                object_with_success = person.user.username
                handle_success(request, title, message, object_with_success)

                return redirect(reverse('usuarios'))

    return render(request, 'main/criar_usuario.html', locals())


@login_required(login_url='/login/')
@person_is_admin
@user_store_match_store_user_edit
def editar_usuario(request, user_id):
    person = request.user.person
    user_edit = User.objects.get(id=user_id)
    person_edit = user_edit.person

    if 'updated' in request.session:
        updated = request.session['updated']
        del request.session['updated']
    else:
        updated = []

    user_password = UserPassword(
        initial={
            'password': '',
        }
    )
    user_form = UserModelForm2(
        initial={
            'username': user_edit.username,
            'first_name': user_edit.first_name,
            'last_name': user_edit.last_name,
            'email': user_edit.email,
        }
    )
    user_is_active_model_form = UserIsActiveModelForm(
        initial={
            'is_active': user_edit.is_active,
        }
    )
    user_is_admin_model_form = UserIsAdminModelForm(
        initial={
            'admin': person_edit.admin,
        }
    )
    person_max_discount_model_form = PersonMaxDiscountModelForm(
        initial={
            'max_discount': person_edit.max_discount,
        }
    )

    errors = {}

    if request.method == 'POST':
        if 'update_user' in request.POST:
            request.session['updated'] = []
            updated = request.session['updated']
            if request.POST['username'] != user_edit.username:
                try:
                    if request.POST['username'] == '':
                        raise Exception('Must not be blank')
                    user_edit.username = request.POST['username']
                    user_edit.save()
                    updated.append('username')
                except Exception as e:
                    if str(e) == 'Must not be blank':
                        errors['username'] = ('<li>'
                                              'Este campo é obrigatório'
                                              '</li>')
                    elif str(e) == ('UNIQUE constraint failed:'
                                    ' auth_user.username'):
                        errors['username'] = ('<li>'
                                              'Já existe um usuário com este'
                                              ' nome'
                                              '</li>')
            if request.POST['first_name'] != user_edit.first_name:
                try:
                    if request.POST['first_name'] == '':
                        raise Exception('Must not be blank')
                    user_edit.first_name = request.POST['first_name'].title()
                    user_edit.save()
                    updated.append('first_name')
                except Exception as e:
                    if str(e) == 'Must not be blank':
                        errors['first_name'] = ('<li>'
                                                'Este campo é obrigatório'
                                                '</li>')
                    else:
                        errors['first_name'] = '<li>Campo inválido</li>'

            if request.POST['last_name'] != user_edit.last_name:
                try:
                    if request.POST['last_name'] == '':
                        raise Exception('Must not be blank')
                    user_edit.last_name = request.POST['last_name'].title()
                    user_edit.save()
                    updated.append('last_name')
                except Exception as e:
                    if str(e) == 'Must not be blank':
                        errors['last_name'] = ('<li>'
                                               'Este campo é obrigatório'
                                               '</li>')
                    else:
                        errors['last_name'] = '<li>Campo inválido</li>'

            if request.POST['email'] != user_edit.email:
                try:
                    if request.POST['email'] == '':
                        raise Exception('Must not be blank')
                    user_edit.email = request.POST['email'].lower()
                    user_edit.save()
                    updated.append('email')
                except Exception as e:
                    if str(e) == 'Must not be blank':
                        errors['email'] = '<li>Este campo é obrigatório</li>'
                    else:
                        errors['email'] = '<li>Campo inválido</li>'

            if int(request.POST['max_discount']) != person_edit.max_discount:
                try:
                    if request.POST['max_discount'] == '':
                        raise Exception('Must not be blank')
                    person_edit.max_discount = request.POST['max_discount']
                    person_edit.save()
                    updated.append('max_discount')
                except Exception as e:
                    if str(e) == 'Must not be blank':
                        errors['last_name'] = ('<li>'
                                               'Este campo é obrigatório'
                                               '</li>')
                    else:
                        errors['last_name'] = '<li>Campo inválido</li>'

            if request.POST.getlist('is_active') == ['on']:
                if user_edit.is_active:
                    pass
                else:
                    user_edit.is_active = True
                    user_edit.save()
                    updated.append('is_active')
            elif request.POST.getlist('is_active') == []:
                if not user_edit.is_active:
                    pass
                else:
                    user_edit.is_active = False
                    user_edit.save()
                    updated.append('is_active')

            if 'admin' in request.POST:
                if person_edit.admin:
                    pass
                else:
                    person_edit.admin = True
                    person_edit.save()
                    updated.append('admin')
            else:
                if not person_edit.admin:
                    pass
                else:
                    person_edit.admin = False
                    person_edit.save()
                    updated.append('admin')

            if (request.POST['password'] != user_edit.password and
                    request.POST['password'] != ''):
                try:
                    user_edit.set_password(request.POST['password'])
                    user_edit.save()
                    updated.append('password')
                except Exception as e:
                    errors.append('password')

            if len(errors) > 0:
                return render(request, 'main/editar_usuario.html', locals())

            return redirect(request.META['HTTP_REFERER'])

        elif 'delete_user' in request.POST:
            request.session['messages'] = {'delete': user_edit.username}
            user_edit.delete()
            return redirect(reverse('usuarios'))

    return render(request, 'main/editar_usuario.html', locals())


@login_required(login_url='/login/')
def cadastro_rapido(request):
    store = request.user.person.store
    form = OrderQuickModelForm()
    errors = {}

    if request.method == 'POST':
        if 'salvar_cadastro' in request.POST:
            try:
                date_post = request.POST['date_wedding']
                date_wedding = Order.str_to_date(date_post)
            except Exception as e:
                errors['date_wedding'] = _('Data de casamento inválida')

            form = OrderQuickModelForm(request.POST)
            if form.is_valid() and len(errors) == 0:
                order = form.save(commit=False)
                order.store = store
                order.date_wedding = date_wedding
                order.save()
                url = reverse('nova_proposta', args=[order.id])
                orderHistoric = OrderHistoric.objects.create(
                    order=order,
                    subject='Cadastro Criado',
                    description='Cadastro criado',
                    status=order.status,
                    type='1',  # follow-up
                )
                return redirect(url)
            else:
                messages = ['Preencher todos os campos']
                render(request, 'main/cadastro_rapido.html', locals())

    return render(request, 'main/cadastro_rapido.html', locals())


@login_required(login_url='/login/')
def nova_proposta(request, order_id):
    '''Criar nova proposta para um cliente.
    '''
    user = request.user
    person = user.person
    store = person.store
    order = Order.objects.get(id=order_id)
    store_from_order = order.store

    if store != store_from_order:
        return redirect(reverse('not_authorized'))

    max_discount = person.max_discount
    one_month_due_date = one_month()

    services_required = store.get_services_required_and_active()
    services_additional = store.get_services_not_required_and_active()

    total = sum(s.price for s in services_required)

    errors = {}
    errors['list_of_errors'] = {}

    if request.method == 'POST':
        if 'avancar' in request.POST:
            checks = request.POST.getlist('service_checkbox')
            if len(checks) < 1:
                errors['error_name'] = _('servicos')
                errors['list_of_errors']['no_service'] = _(
                    'Nenhum serviço selecionado'
                )
                return render(request, 'main/nova_proposta.html', locals())
            else:
                services = []
                for check in checks:
                    services.append(
                        Product.objects.get(
                            name=check,
                            store=store
                        )
                    )

            discount = 0
            check_discount = request.POST.getlist('discount_check')
            if len(check_discount) > 0:
                discount_percentage_post = request.POST['discount_percentage']
                try:
                    discount_percentage_post = int(discount_percentage_post)
                except Exception:
                    discount_percentage_post = 0
                print(discount_percentage_post)
                discount_percentage = round(float(discount_percentage_post), 2)
                if not person.admin:
                    if discount_percentage > max_discount:
                        errors['error_name'] = 'desconto'
                        errors['list_of_errors']['discount'] = _(
                            _('Desconto maior do que o permitido para o usuário')
                        )
                        return render(
                            request,
                            'main/nova_proposta.html',
                            locals()
                        )
                else:
                    discount = discount_percentage

            due_date_post = request.POST['due_date']
            if due_date_post != '':
                try:
                    due_date = Order.str_to_date(due_date_post)
                except OverflowError:
                    errors['error_name'] = 'due_date'
                    errors['list_of_errors']['due_date'] = _(
                        _('Data de vencimento inválida. '
                            'Verifique se você inseriu uma data de vencimento '
                            'válida.')
                    )
                    due_date = None
                except ValueError:
                    errors['error_name'] = 'due_date'
                    errors['list_of_errors']['due_date'] = _(
                        _('Data de vencimento inválida. '
                            'Verifique se você inseriu uma data de vencimento '
                            'válida.')
                    )
                    due_date = None
                if not due_date:
                    if request.POST['type'] == 'proposal_alter':
                        proposal = Proposal.objects.get(id=request.POST['proposal_id'])
                        services_in_proposal = proposal.services()
                    return render(
                        request,
                        'main/nova_proposta.html',
                        locals()
                    )
                if due_date < now().date():
                    errors['error_name'] = 'due_date'
                    errors['list_of_errors']['due_date'] = _(
                        _('Data de vencimento inválida. '
                            'Verifique se você inseriu uma data de vencimento '
                            'anterior a data atual.')
                    )
                    if request.POST['type'] == 'proposal_alter':
                        proposal = Proposal.objects.get(id=request.POST['proposal_id'])
                        services_in_proposal = proposal.services()
                    return render(
                        request,
                        'main/nova_proposta.html',
                        locals()
                    )
            else:
                due_date = one_month()

            proposal = Proposal.objects.create(
                order=order,
                attendant=person,
                due_date=due_date
            )

            proposal.discount = discount
            proposal.save()

            for service in services:
                ProposalDetail.objects.create(proposal=proposal,
                                              service=service)

            # Cria historico
            order_historic = OrderHistoric.objects.create(
                order=order,
                proposal=proposal,
                subject='Nova proposta numero %s' % (proposal.id),
                description='Criado nova proposta numero %s' % (proposal.id),
                status=order.status,
                type='0',  # proposta
            )

            url = reverse('proposta_detalhes', args=[order.id, proposal.id])
            return redirect(url)

    return render(request, 'main/nova_proposta.html', locals())


@login_required(login_url='/login/')
@user_store_match_store_order
def alterar_proposta(request, order_id, proposalId):
    '''Criar nova proposta baseada em uma já existente.
    '''
    user = request.user
    person = user.person
    store = person.store
    proposal = Proposal.objects.get(id=proposalId)
    order = proposal.order
    store_from_order = order.store
    today = datetime.date.today()
    services = proposal.details()
    discount = proposal.discount
    max_discount = person.max_discount
    services_in_proposal = proposal.services()
    services_required = Product.objects.all().filter(
        store=store,
        required=True,
        active=True
    )
    services_additional = Product.objects.all().filter(
        store=store,
        required=False,
        active=True
    )
    total = sum(s.price for s in services_required)

    errors = {}
    errors['list_of_errors'] = {}

    due_date = (
        str(proposal.due_date.day) +
        "/" +
        str(proposal.due_date.month) +
        "/" +
        str(proposal.due_date.year)
    )

    return render(request, 'main/nova_proposta.html', locals())


@login_required(login_url='/login/')
@user_store_match_store_order
def proposta_dados_adicionais(request, order_id, proposalId):
    '''Inserir dados adicionais da proposta.
    '''
    user = request.user
    person = user.person
    store = person.store
    proposal = Proposal.objects.get(id=proposalId)
    order = proposal.order
    store_from_order = order.store

    proposal = Proposal.objects.get(id=proposalId)
    today = datetime.date.today()
    one_week = today + relativedelta(days=7)

    form_order = OrderModelForm2(instance=order)

    messages = {}
    messages['errors'] = {}
    messages['updated'] = {}

    if request.method == 'POST':
        if 'avancar_proposta' in request.POST:
            fields = order.get_all_fields()
            POST = request.POST

            for field in fields:
                if field in request.POST:
                    if POST[field] != '':
                        if (field == 'date_test' or
                                field == 'date_wedding' or
                                field == 'date_return'):
                            try:
                                value = Order.str_to_date(POST[field])
                                if value != getattr(order, field):
                                    try:
                                        setattr(order, field, value)
                                        messages['updated'][field] = _(
                                            'Campo atualizado'
                                        )
                                    except Exception:
                                        messages['errors'][field] = _(
                                            'Data inválida'
                                        )
                            except Exception:
                                messages['errors'][field] = _('Campo inválido')
                        elif (field == 'time_arrival' or
                                field == 'time_leaving' or
                                field == 'time_test'):
                            try:
                                value = Order.str_to_time(POST[field])
                                if value != getattr(order, field):
                                    try:
                                        setattr(order, field, value)
                                        messages['updated'][field] = _(
                                            'Campo atualizado'
                                        )
                                    except Exception:
                                        messages['errors'][field] = _(
                                            'Campo inválido'
                                        )
                            except Exception:
                                messages['errors'][field] = _('Campo inválido')
                        else:
                            try:
                                value = POST[field]
                                if value != getattr(order, field):
                                    try:
                                        setattr(order, field, value)
                                        messages['updated'][field] = _(
                                            'Campo atualizado'
                                        )
                                    except Exception:
                                        messages['errors'][field] = _(
                                            'Campo inválido'
                                        )
                            except Exception:
                                messages['errors'][field] = _('Campo inválido')
                    else:
                        messages['errors'][field] = _('Campo obrigatório')

            order.save()

            if len(messages['errors']) > 0:
                form_order = OrderModelForm2(instance=order)
                return render(
                    request,
                    'main/proposta_dados_adicionais.html',
                    locals()
                )

            form_order = OrderModelForm2(instance=order)
            url = reverse('proposta_pagamento', args=[order.id, proposal.id])
            return redirect(url)

    return render(request, 'main/proposta_dados_adicionais.html', locals())


@login_required(login_url='/login/')
@user_store_match_store_order
def proposta_detalhes(request, order_id, proposalId):
    '''Ver detalhes da proposta, imprimir, enviar, alterar
    e prosseguir para o contrato
    '''
    user = request.user
    order = Order.objects.get(id=order_id)
    store = order.store
    store_self = user.person.store
    proposal = Proposal.objects.get(id=proposalId)
    proposaldetails = proposal.details()
    today = datetime.date.today()
    payment_methods = store.get_payment_methods()

    success = {}
    errors = {}

    if request.method == 'POST':
        if 'enviar_proposta' in request.POST:
            today = datetime.date.today()
            date_post = request.POST['date_return']
            try:
                date_return = Order.str_to_date(date_post)
            except Exception as e:
                errors['date_return'] = _('Data de retorno inválida')
                return render(request, 'main/proposta_detalhes.html', locals())

            if store.email:
                sender = store.email
            else:
                sender = 'diadanoiva@bruno-lucena.com.br'

            proposal.send_proposal(
                request,
                date_return,
                testing=False,
                sender=sender
            )

            success['proposta'] = True

            return render(request, 'main/proposta_detalhes.html', locals())

    elif request.method == 'GET':
        return render(request, 'main/proposta_detalhes.html', locals())

    return render(request, 'main/proposta_detalhes.html', locals())


@login_required(login_url='/login/')
@user_store_match_store_order
def proposta_contrato(request, order_id, proposalId):
    '''Visualizar e enviar contrato.
    '''
    user = request.user
    person = user.person
    store = person.store
    proposal = Proposal.objects.get(id=proposalId)
    order = proposal.order
    store_from_order = order.store
    proposaldetails = proposal.details()
    installments = order.get_installments()
    try:
        contract = store.contract
    except Contract.DoesNotExist:
        contract = Contract.objects.get_or_create(
            store=store,
            title=store.name,
            header="Header",
            footer="Footer"
        )[0]

    articles_db = contract.get_articles()
    today = datetime.date.today()

    df = DateFormat(order.date_wedding)
    df.format(get_format('DATE_FORMAT'))

    df2 = DateFormat(order.date_test)
    df2.format(get_format('DATE_FORMAT'))

    data_atual = today.strftime('%d/%m/%Y')

    try:
        demais_parcelas = float(installments[1].total),
    except IndexError:
        demais_parcelas = 00.00

    articles = {}
    args = {
        'contratada': str(store.company_name),
        'empresa': str(store.company_name),
        'endereco': str(store.address),
        'cnpj': str(store.cnpj),
        'proprietario': str(store.owner.get_full_name()),
        'rg_proprietario': str(store.id_owner),
        'cpf_proprietario': str(store.cpf_owner),
        'contratante': str(order.get_full_name()),
        'contratante_rg': str(order.rg),
        'contratante_cpf': str(order.cpf),
        'contratante_endereco': str(order.get_full_address()),
        'data_casamento': str(df.format('d/m/Y')),
        'horario_chegada': str(order.time_arrival.strftime('%H:%Mh')),
        'horario_saida': str(order.time_leaving.strftime('%H:%Mh')),
        'contratante_telefone': str(order.phone),
        'contratante_email': str(order.email),
        'cabeleireiro': str(order.professional_hair),
        'maquiador': str(order.professional_make_up),
        'data_teste': str(df2.format('d/m/Y')),
        'horario_teste': str(order.time_test.strftime('%H:%Mh')),
        'pagamento': str(order.payment_type),
        'parcelas_quantidade': str(order.installments),
        'desconto_porcentagem': str(order.discount),
        'desconto_total': str(order.discount_total),
        'condicoes_pagamento': proposal.get_payment_conditions_text(),
        'total_com_desconto': str(round(float(order.total2), 2)),
        'produtos': str(proposal.get_services_div()),
        'entrada_total': str(installments[0].total),
        'demais_parcelas': str(order.installments - 1),
        'demais_parcelas_valor': str(demais_parcelas),
        'forma_de_pagamento': str(proposal.payment_method),
        'dados_bancarios': str(order.store.owner.person.get_bank_accounts_div()),
        'data_atual': str(data_atual),

    }
    for article in articles_db:
        articles[article.id] = {}
        articles[article.id]['title'] = article.render_content(
            content='title',
            args=args
        )
        articles[article.id]['content'] = article.render_content(
            content='content',
            args=args
        )

    title = contract.render_content(content='title', args=args)
    header = contract.render_content(content='header', args=args)
    footer = contract.render_content(content='footer', args=args)
    success = {}
    errors = {}

    if request.method == 'POST':
        if 'enviar_contrato' in request.POST:
            today = datetime.date.today()
            date_post = request.POST['date_return']
            try:
                date_return = Order.str_to_date(date_post)
            except Exception as e:
                errors['date_return'] = _('Data de retorno inválida')
                return render(request, 'main/proposta_contrato.html', locals())

            if store.email:
                sender = store.email
            else:
                sender = 'diadanoiva@bruno-lucena.com.br'

            proposal.send_contract(
                request,
                date_return,
                testing=False,
                subject='Contrato Dia da Noiva',
                type_pdf='contract',
                sender=sender
            )

            success['contrato'] = True

            return render(request, 'main/proposta_contrato.html', locals())

    return render(request, 'main/proposta_contrato.html', locals())


def contrato_gerar_pdf(request, proposalId):
    '''Página de acesso para API pdfy.
    '''
    proposal = Proposal.objects.get(id=proposalId)
    order = proposal.order
    store = order.store
    proposaldetails = proposal.details()
    installments = order.get_installments()
    try:
        contract = store.contract
    except Contract.DoesNotExist:
        contract = Contract.objects.get_or_create(
            store=store,
            title=store.name,
            header="Header",
            footer="Footer"
        )[0]

    articles_db = contract.get_articles()
    today = datetime.date.today()

    df = DateFormat(order.date_wedding)
    df.format(get_format('DATE_FORMAT'))

    df2 = DateFormat(order.date_test)
    df2.format(get_format('DATE_FORMAT'))

    data_atual = today.strftime('%d/%m/%Y')

    try:
        demais_parcelas = float(installments[1].total),
    except IndexError:
        demais_parcelas = 00.00

    articles = {}
    args = {
        'empresa': str(store.company_name),
        'endereco': str(store.address),
        'cnpj': str(store.cnpj),
        'proprietario': str(store.owner.get_full_name()),
        'rg_proprietario': str(store.id_owner),
        'cpf_proprietario': str(store.cpf_owner),
        'contratante': str(order.get_full_name()),
        'contratante_rg': str(order.rg),
        'contratante_cpf': str(order.cpf),
        'contratante_endereco': str(order.get_full_address()),
        'data_casamento': str(df.format('d/m/Y')),
        'horario_chegada': str(order.time_arrival.strftime('%H:%Mh')),
        'horario_saida': str(order.time_leaving.strftime('%H:%Mh')),
        'contratante_telefone': str(order.phone),
        'contratante_email': str(order.email),
        'cabeleireiro': str(order.professional_hair),
        'maquiador': str(order.professional_make_up),
        'data_teste': str(df2.format('d/m/Y')),
        'horario_teste': str(order.time_test.strftime('%H:%Mh')),
        'pagamento': str(order.payment_type),
        'parcelas_quantidade': str(order.installments),
        'desconto_porcentagem': str(order.discount),
        'desconto_total': str(order.discount_total),
        'condicoes_pagamento': proposal.get_payment_conditions_text(),
        'total_com_desconto': str(round(float(order.total2), 2)),
        'produtos': str(proposal.get_services_div()),
        'entrada_total': str(installments[0].total),
        'demais_parcelas': str(order.installments - 1),
        'demais_parcelas_valor': str(demais_parcelas),
        'forma_de_pagamento': str(proposal.payment_method),
        'dados_bancarios': str(order.store.owner.person.get_bank_accounts_div()),
        'data_atual': str(data_atual),
    }
    for article in articles_db:
        articles[article.id] = {}
        articles[article.id]['title'] = article.render_content(
            content='title',
            args=args
        )
        articles[article.id]['content'] = article.render_content(
            content='content',
            args=args
        )

    title = contract.render_content(content='title', args=args)
    header = contract.render_content(content='header', args=args)
    footer = contract.render_content(content='footer', args=args)
    success = {}
    errors = {}

    if request.method == 'POST':
        if 'enviar_contrato' in request.POST:
            today = datetime.date.today()
            date_post = request.POST['date_return']
            try:
                date_return = Order.str_to_date(date_post)
            except Exception as e:
                errors['date_return'] = _('Data de retorno inválida')
                return render(request, 'main/proposta_contrato.html', locals())

            if store.email:
                sender = store.email
            else:
                sender = 'diadanoiva@bruno-lucena.com.br'

            proposal.send_contract(
                request,
                date_return,
                testing=False,
                subject='Contrato Dia da Noiva',
                type_pdf='contract',
                sender=sender
            )

            success['contrato'] = True

            return render(request, 'main/proposta_contrato.html', locals())

    return render(request, 'main/contrato_imprimir.html', locals())


@check_token_access
def proposta_gerar_pdf(request, proposal_id, token_access):
    '''Página de acesso para API pdfy.
    '''
    proposal = Proposal.objects.get(id=proposal_id)
    order = proposal.order
    store = order.store
    proposaldetails = proposal.details()
    payment_methods = store.get_payment_methods()
    today = datetime.date.today()
    payment_due = proposal.due_date

    return render(request, 'main/proposta_imprimir.html', locals())


@login_required(login_url='/login/')
@user_store_match_store_order
def proposta_imprimir(request, order_id, proposal_id):
    '''Imprimir poposta.
    '''
    proposal = Proposal.objects.get(id=proposal_id)
    order = proposal.order
    store = order.store
    proposaldetails = proposal.details()
    payment_methods = store.get_payment_methods()
    today = datetime.date.today()
    payment_due = proposal.due_date

    return render(request, 'main/proposta_imprimir.html', locals())


@login_required(login_url='/login/')
@user_store_match_store_order
def contrato_imprimir(request, order_id, proposalId):
    '''Imprimir contrato.
    '''
    proposal = Proposal.objects.get(id=proposalId)
    order = proposal.order
    store = order.store
    proposaldetails = proposal.details()
    installments = order.get_installments()
    try:
        contract = store.contract
    except Contract.DoesNotExist:
        contract = Contract.objects.get_or_create(
            store=store,
            title=store.name,
            header="Header",
            footer="Footer"
        )[0]

    articles_db = contract.get_articles()
    today = datetime.date.today()

    df = DateFormat(order.date_wedding)
    df.format(get_format('DATE_FORMAT'))

    df2 = DateFormat(order.date_test)
    df2.format(get_format('DATE_FORMAT'))

    data_atual = today.strftime('%d/%m/%Y')

    try:
        demais_parcelas = float(installments[1].total),
    except IndexError:
        demais_parcelas = 00.00

    articles = {}
    args = {
        'empresa': str(store.company_name),
        'endereco': str(store.address),
        'cnpj': str(store.cnpj),
        'proprietario': str(store.owner.get_full_name()),
        'rg_proprietario': str(store.id_owner),
        'cpf_proprietario': str(store.cpf_owner),
        'contratante': str(order.get_full_name()),
        'contratante_rg': str(order.rg),
        'contratante_cpf': str(order.cpf),
        'contratante_endereco': str(order.get_full_address()),
        'data_casamento': str(df.format('d/m/Y')),
        'horario_chegada': str(order.time_arrival.strftime('%H:%Mh')),
        'horario_saida': str(order.time_leaving.strftime('%H:%Mh')),
        'contratante_telefone': str(order.phone),
        'contratante_email': str(order.email),
        'cabeleireiro': str(order.professional_hair),
        'maquiador': str(order.professional_make_up),
        'data_teste': str(df2.format('d/m/Y')),
        'horario_teste': str(order.time_test.strftime('%H:%Mh')),
        'pagamento': str(order.payment_type),
        'parcelas_quantidade': str(order.installments),
        'desconto_porcentagem': str(order.discount),
        'desconto_total': str(order.discount_total),
        'condicoes_pagamento': proposal.get_payment_conditions_text(),
        'total_com_desconto': str(round(float(order.total2), 2)),
        'produtos': str(proposal.get_services_div()),
        'entrada_total': str(installments[0].total),
        'demais_parcelas': str(order.installments - 1),
        'demais_parcelas_valor': str(demais_parcelas),
        'forma_de_pagamento': str(proposal.payment_method),
        'dados_bancarios': str(order.store.owner.person.get_bank_accounts_div()),
        'data_atual': str(data_atual),
    }
    for article in articles_db:
        articles[article.id] = {}
        articles[article.id]['title'] = article.render_content(
            content='title',
            args=args
        )
        articles[article.id]['content'] = article.render_content(
            content='content',
            args=args
        )

    title = contract.render_content(content='title', args=args)
    header = contract.render_content(content='header', args=args)
    footer = contract.render_content(content='footer', args=args)
    success = {}
    errors = {}

    if request.method == 'POST':
        if 'enviar_contrato' in request.POST:
            today = datetime.date.today()
            date_post = request.POST['date_return']
            try:
                date_return = Order.str_to_date(date_post)
            except Exception as e:
                errors['date_return'] = _('Data de retorno inválida')
                return render(request, 'main/proposta_contrato.html', locals())

            if store.email:
                sender = store.email
            else:
                sender = 'diadanoiva@bruno-lucena.com.br'

            proposal.send_contract(
                request,
                date_return,
                testing=False,
                subject='Contrato Dia da Noiva',
                type_pdf='contract',
                sender=sender
            )

            success['contrato'] = True

            return render(request, 'main/proposta_contrato.html', locals())

    return render(request, 'main/contrato_imprimir.html', locals())


@login_required(login_url='/login/')
def propostas(request):
    store = request.user.person.store
    orders = Order.objects.filter(store=store)
    proposals = []
    for order in orders:
        try:
            proposals.append(order.get_proposals().latest('date'))
        except Exception:
            pass

    return render(request, 'main/propostas.html', locals())


@login_required(login_url='/login/')
@user_store_match_store_order
def cliente(request, order_id):
    '''Ver e editar detalhes de um cliente,
    ver e adicionar novas propostas,
    ver e adicionar histórico.
    '''
    user = request.user
    order = Order.objects.get(id=order_id)
    store = order.store
    store_self = user.person.store
    historics = order.get_historics()
    proposals = order.get_proposals()

    form_order = OrderUpdateModelForm(instance=order)
    form_historic = OrderHistoricModelForm(initial={'order': order})

    updated = {}
    updated['update_order'] = {}
    updated['save_historic'] = {}
    errors = {}
    messages = {}
    messages['updated'] = {}
    messages['errors'] = {}

    if request.method == 'POST':
        if 'save_historic' in request.POST:
            form_historic = OrderHistoricModelForm(request.POST)
            if form_historic.is_valid():
                try:
                    proposal_id = request.POST['proposal']
                    proposal = Proposal.objects.get(id=proposal_id)
                except Exception:
                    pass
                p = form_historic.save(commit=False)
                p.order = order
                try:
                    p.proposal = proposal
                except Exception:
                    pass
                p.save()

                updated['save_historic']['historic_id'] = p.proposal
                updated['save_historic']['subject'] = p.subject
                updated['save_historic']['description'] = p.description

                return render(request, 'main/cliente.html', locals())
            else:
                for error in form_historic.errors:
                    errors[error] = '<li>Campo inválido</li>'

                return render(request, 'main/cliente.html', locals())

        if 'update_order' in request.POST:
            fields = order.get_all_fields()
            POST = request.POST

            for field in fields:
                if field in request.POST:
                    if POST[field] != '':
                        if (field == 'date_test' or
                                field == 'date_wedding' or
                                field == 'date_return'):
                            try:
                                value = Order.str_to_date(POST[field])
                                if value != getattr(order, field):
                                    try:
                                        setattr(order, field, value)
                                        messages['updated'][field] = _(
                                            'Campo atualizado'
                                        )
                                    except Exception:
                                        messages['errors'][field] = _(
                                            'Data inválida'
                                        )
                            except Exception:
                                messages['errors'][field] = _('Campo inválido')
                        elif (field == 'time_arrival' or
                                field == 'time_leaving' or
                                field == 'time_test'):
                            try:
                                value = Order.str_to_time(POST[field])
                                if value != getattr(order, field):
                                    try:
                                        setattr(order, field, value)
                                        messages['updated'][field] = _(
                                            'Campo atualizado'
                                        )
                                    except Exception:
                                        messages['errors'][field] = _(
                                            'Campo inválido'
                                        )
                            except Exception:
                                messages['errors'][field] = _('Campo inválido')
                        else:
                            try:
                                value = POST[field]
                                if value != getattr(order, field):
                                    try:
                                        setattr(order, field, value)
                                        messages['updated'][field] = _(
                                            'Campo atualizado'
                                        )
                                    except Exception:
                                        messages['errors'][field] = _(
                                            'Campo inválido'
                                        )
                            except Exception:
                                messages['errors'][field] = _('Campo inválido')
                    else:
                        messages['errors'][field] = _('Campo obrigatório')

            order.save()

            if len(messages['errors']) > 0:
                form_order = OrderUpdateModelForm(instance=order)
                return render(request, 'main/cliente.html', locals())

            form_order = OrderUpdateModelForm(instance=order)
            return render(request, 'main/cliente.html', locals())

    return render(request, 'main/cliente.html', locals())


@login_required(login_url='/login/')
@user_store_match_store_order
def proposta_pagamento(request, order_id, proposalId):
    '''Inserir dados de pagamento na proposta.
    '''
    user = request.user
    person = user.person
    store = person.store
    proposal = Proposal.objects.get(id=proposalId)
    order = proposal.order
    store_from_order = order.store
    payment_methods = store.get_payment_methods()
    installments_qt = 1
    total = proposal.total2
    today = datetime.date.today()

    messages = {}
    messages['errors'] = {}

    if request.method == 'POST':
        if 'avancar_proposta' in request.POST:
            installments_qt = int(request.POST['installments_qt'])
            order.installments = installments_qt
            order.save()

            installments_dict = {}
            for i in range(installments_qt):
                b = i + 1
                key = str(b)
                try:
                    field = 'installment{0}'.format(str(key))
                    field2 = 'installment{0}date'.format(str(key))
                    if field in request.POST:
                        installments_dict[field] = {}
                        installments_dict[field]['value'] = round(
                            float(
                                request.POST[field]
                            ),
                            2
                        )
                        installments_dict[field]['date'] = request.POST[field2]
                except Exception as e:
                    pass

            installment_check_value = 0
            try:
                for i in installments_dict.items():
                    value = round(float(i[1]['value']), 2)
                    installment_check_value += value
            except Exception:
                messages['errors']['installments'] = _(
                    'Valor das parcelas não coincide com o valor total'
                )
                return render(
                    request,
                    'main/proposta_pagamento.html',
                    locals()
                )

            if (round(float(installment_check_value), 2) !=
                    round(float(total), 2)):
                messages['errors']['installments'] = _(
                    'Valor das parcelas não coincide com o valor total'
                )
                return render(
                    request,
                    'main/proposta_pagamento.html',
                    locals()
                )

            try:
                order.set_installments(installments_dict)
            except Exception as e:
                messages['errors']['installments'] = _(
                    'Valor das parcelas não coincide com o valor total'
                )
                print(e)
                return render(
                    request,
                    'main/proposta_pagamento.html',
                    locals()
                )

            payment_type = request.POST['payment_type']
            payment_type = json.loads(payment_type)
            for key, value in payment_type.items():
                payment_method = PaymentMethods.objects.get(id=key)

            proposal.payment_method = payment_method.payment_method.name
            proposal.deal = True
            proposal.save()
            url = reverse('proposta_contrato', args=[order.id, proposal.id])
            return redirect(url)

    return render(request, 'main/proposta_pagamento.html', locals())


def logout_custom(request):
    logout(request)
    return redirect('/')


@login_required(login_url='/login/')
def accounts_profile(request):
    return redirect('/')
