��    7      �      �      �     �  	   �     �     �     �  
   �     �     �     �  	     
        $     +     ;  	   M     W  	   `     j     �  f   �  X        `  1   o     �  	   �  1   �     �     �          	          $  
   *     5     Q     V     e     v     �     �     �  
   �     �  S   �     !     *  	   J  '   T     |  Y   �  2   �               /  o  8     �	     �	     �	     �	  	   �	     �	     �	     �	     
     
     
     #
     ,
     9
     G
     O
  	   X
     b
     w
  G   �
  6   �
       *        E     K  '   S     {     �     �  
   �     �     �     �     �     �  
   �     �     �     	               .     7  F   >     �     �     �  #   �     �  @   �  4     	   R     \     n   Aguardando Fechar Contrato Agência  CNPJ CNPJ ou CPF  CPF do Dono Cabeçalho Campo atualizado Campo inválido Campo obrigatório Cláusula Cláusulas Conta  Conta Bancária Contas Bancárias Conteúdo Contrato Contratos Data de casamento inválida Data de retorno inválida Data de vencimento inválida. Verifique se você inseriu uma data de vencimento anterior a data atual. Data de vencimento inválida. Verifique se você inseriu uma data de vencimento válida. Data inválida Desconto maior do que o permitido para o usuário Dono Endereço Endereço completo como deve aparecer no contrato Enviado Esta conta está inativa. Loja LojaTelefone LojaTelefones Lojas Negociando Nenhum serviço selecionado Nome Nome Abreviado Nome Empresarial Números e ponto somente. Obrigatório Password Preço Padrão (R$) RG do Dono Rodapé Se não for encontrado um preço específico para o dia atual, este será utilizado Serviço Serviço obrigatório no pacote Serviços Somente iniciais. Ex: Bruno Lucena = BL Título Usuário ou senha inválidos.Ambos os campos podem diferenciar maiúsculas e minúsculas. Valor das parcelas não coincide com o valor total Venda Perdida Venda Realizada servicos Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2017-09-30 15:12-0300
PO-Revision-Date: 2017-09-30 15:38-0300
Last-Translator: b'  <bruno.lucenac@gmail.com>'
Language-Team: LANGUAGE <LL@li.org>
Language: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Translated-Using: django-rosetta 0.7.13
 Waiting contract deal Agency  CNPJ CNPJ or CPF  Owner CPF Header Field updated Invalid field Required field Clause Clauses Account  Bank Account Back Accounts Content Contract Contracts Invalid wedding date Invalid date of return Invalid due date. See if you inserted a due date lower than actual date Invalid due date. See if you inserted a valid due date Invalid date Discount higher than allowed for this user Owner Address Address as it should appear on contract Sent Inative account Store StorePhone StorePhones Stores Negotiating No service selected Name Short name Business Name Numbers and dots only. Required Password Standard Price ($) Owner ID Footer If a specific price is not found for the actual day, this will be used Service Required service on package Services Only initial. Ex: Bruno Lucena = BL Title Invalid username or password. Both fields can be case-sensitive. Installments value doesn't coincide with total value Sell lost Sell accomplished services 