from django.contrib import admin

from main.models import *


class ContractArticlesInline(admin.TabularInline):
    model = ContractArticles
    extra = 0


@admin.register(Contract)
class ContractAdmin(admin.ModelAdmin):
    list_display = ['id', 'store']
    ordering = ['id', 'store']
    inlines = [ContractArticlesInline]


@admin.register(EmailRecipients)
class EmailRecipientsAdmin(admin.ModelAdmin):
    list_display = ['email', 'main']


class OrderInstallmentsInline(admin.TabularInline):
    model = OrderInstallments
    extra = 0


class OrderHistoricInline(admin.TabularInline):
    model = OrderHistoric
    extra = 0
    ordering = ['-date']


class ProposalInline(admin.TabularInline):
    model = Proposal
    extra = 0
    fields = ['deal', 'date', 'attendant', 'proposal_sent', 'contract_sent']
    ordering = ['-date']


@admin.register(Order)
class OrderAdmin(admin.ModelAdmin):
    list_display = ['id', 'full_name', 'store', 'proposal_deal',
                    'total', 'discount', 'total2', 'date_created',
                    'date_wedding', 'date_return', 'status']
    list_filter = ['name', 'lastname', 'status']
    ordering = ['status', 'id', 'date_return', 'name', 'lastname']
    inlines = [ProposalInline, OrderInstallmentsInline, OrderHistoricInline]


@admin.register(OrderHistoric)
class OrderHistoricAdmin(admin.ModelAdmin):
    list_display = ['order', 'proposal', 'subject', 'date',
                    'date_return', 'status', 'type']
    ordering = ['date_return', 'order']


@admin.register(OrderNumber)
class OrderNumberAdmin(admin.ModelAdmin):
    list_display = ['number']


@admin.register(PaymentMethods)
class PaymentMethodsAdmin(admin.ModelAdmin):
    list_display = ['store', 'payment_method', 'installments']
    ordering = ['store', 'payment_method', 'installments']


@admin.register(PaymentMethodsSystem)
class PaymentMethodsSystemAdmin(admin.ModelAdmin):
    list_display = ['name', 'type', 'description']
    ordering = ['name', 'type', 'description']


class PersonBankAccountInline(admin.TabularInline):
    model = BankAccount
    extra = 0


@admin.register(Person)
class PersonAdmin(admin.ModelAdmin):
    list_display = ['user', 'store', 'role', 'max_discount', 'admin']
    ordering = ['user']
    inlines = [PersonBankAccountInline]


class ProductPriceInline(admin.TabularInline):
    model = ProductPrice
    extra = 0
    ordering = ['date_start', 'date_end']


@admin.register(Product)
class ProductAdmin(admin.ModelAdmin):
    list_display = ['id', 'name', 'store', 'price', 'required', 'active']
    list_filter = ['store']
    ordering = ['-id', 'name']
    inlines = [ProductPriceInline]


class ProposalDetailInline(admin.TabularInline):
    model = ProposalDetail
    extra = 0


@admin.register(Proposal)
class ProposalAdmin(admin.ModelAdmin):
    list_display = ['id', 'order', 'due_date', 'proposal_sent',
                    'contract_sent', 'proposal_date_sent',
                    'contract_date_sent', 'total', 'discount',
                    'total2', 'attendant']
    ordering = ['-id', 'attendant']
    inlines = [ProposalDetailInline]


class StorePhonesInline(admin.TabularInline):
    model = StorePhones
    extra = 0


@admin.register(Store)
class StoreAdmin(admin.ModelAdmin):
    list_display = ['name', 'short_name', 'owner', ]
    ordering = ['name']
    inlines = [StorePhonesInline]


@admin.register(TokenAccess)
class TokenAccessAdmin(admin.ModelAdmin):
    list_display = ['token', 'due_time']
    ordering = ['due_time']
