import datetime

from django.contrib.auth import logout

from main.models import User, Person, Store


def page_configuration(request):
    user = request.user

    if user.is_authenticated():
        if not user.is_active:
            logout(request)

        try:
            store = user.store
        except Exception:
            store = Store.objects.get_or_create(
                name="default_store_2",
                owner=User.objects.get_or_create(
                    username='default_store_2'
                )[0]
            )[0]
            user.store = store

        users_len = len(User.objects.all())

        try:
            person = user.person
        except Exception:
            person = Person.objects.create(user=user, store=store)
            person.save()

        today = datetime.date.today()

        current_page = request.get_full_path()

        my_context = {
            'user': user,
            'users_len': users_len,
            'person': person,
            'store': store,
            'today': today,
            'current_page': current_page
        }

        # my_context = dict()

        return my_context

    else:
        my_context = {}

        return my_context
