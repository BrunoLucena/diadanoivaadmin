import os
import datetime
import random

from binascii import hexlify

from django.db import IntegrityError
from django.core.exceptions import ObjectDoesNotExist
from django.utils.timezone import now

import main


def check_session_form(request):
    if 'success' in request.session:
        success = request.session['success']
        del request.session['success']
    else:
        success = {}

    if 'error' in request.session:
        error = request.session['error']
        del request.session['error']
    else:
        error = {}

    if 'fields_updated' in request.session:
        fields_updated = request.session['fields_updated']
        del request.session['fields_updated']
    else:
        fields_updated = {}

    if 'fields_with_error' in request.session:
        fields_with_error = request.session['fields_with_error']
        del request.session['fields_with_error']
    else:
        fields_with_error = {}

    session = {
        'success': success,
        'error': error,
        'fields_updated': fields_updated,
        'fields_with_error': fields_with_error
    }

    return session


def create_order_number():
    list = []
    for n in range(0, 10):
        list.append(n)
    number = None
    final = ''
    while number is None:
        random.shuffle(list)
        for n in list:
            final += str(n)
        number = final
    return number


def create_hash():
    token = hexlify(os.urandom(64))
    check = False

    while not check:
        try:
            main.models.TokenAccess.objects.get(token=token)
            check = False
        except ObjectDoesNotExist:
            check = True

    return token


def create_token_access():
    '''Create token API pdf'''
    token = None

    while token is None:
        try:
            main.models.TokenAccess.objects.create(
                token=hexlify(os.urandom(64))
            )
        except IntegrityError:
            token = None

    return hexlify(os.urandom(64))


def five_minutes_from_now():
    '''Create datetime object five minutes on the future'''
    five_minutes_from_now = now() + datetime.timedelta(minutes=5)

    return five_minutes_from_now


def one_month():
    return now() + datetime.timedelta(days=30)


def ponto_para_virgula(string):
    new = string.replace('.', ',')
    return str(new)


def underscores_to_scores(arg):
    return arg.replace("_", "-").replace("'", "")


def underscores_to_spaces(arg):
    return arg.replace('_', ' ').replace("'", "")


def virgula_para_ponto(string):
    new = float(string.replace(',', '.'))
    return str(new)
