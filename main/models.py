# -*- coding: utf-8 -*-

import os
import datetime
import json
import re
import requests

from django.conf import settings
from django.contrib.auth.models import User
from django.core import serializers
from django.core.mail import EmailMultiAlternatives
from django.core.files.base import ContentFile
from django.core.files.storage import FileSystemStorage
from django.core.validators import (
    RegexValidator,
    MaxValueValidator,
    MinValueValidator
)
from django.db import models
from django.template.loader import render_to_string
from django.utils.crypto import get_random_string
from django.utils.dateformat import DateFormat
from django.utils.timezone import now
from django.utils.translation import gettext as _
from smtplib import SMTPException

from main.functions import *


validator = RegexValidator(r'[0-9.]', _('Números e ponto somente.'))

FARE_TYPES = (
    ('pac', 'PAC'),
    ('sedex', 'SEDEX'),
    ('sedex10', 'SEDEX10')
)

HISTORIC_TYPE = (
    ('0', 'proposta'),
    ('1', 'follow-up'),
)

PRODUCT_TYPES = (
    ('produto', 'Produto'),
    ('produto-servico', 'Produto-Servico'),
    ('servico', 'Servico')
)

ORDER_STATUS = (
    ('0', _('Enviado')),
    ('1', _('Negociando')),
    ('2', _('Aguardando Fechar Contrato')),
    ('3', _('Venda Realizada')),
    ('4', _('Venda Perdida')),
)

PAYMENT_METHOD_TYPE = (
    ('0', 'debit'),
    ('1', 'credit'),
)

DATE_FORMATS = [
    # '2006-10-25', '10/25/2006', '10/25/06'
    '%Y-%m-%d', '%m/%d/%Y', '%m/%d/%y',
    # 'Oct 25 2006', 'Oct 25, 2006'
    '%b %d %Y', '%b %d, %Y',
    # '25 Oct 2006', '25 Oct, 2006'
    '%d %b %Y', '%d %b, %Y',
    # 'October 25 2006', 'October 25, 2006'
    '%B %d %Y', '%B %d, %Y',
    # '25 October 2006', '25 October, 2006'
    '%d %B %Y', '%d %B, %Y',
]


def one_month():
    return now() + datetime.timedelta(days=30)


def one_week():
    return now() + datetime.timedelta(days=7)


class Store(models.Model):
    '''All persons must be related to a store'''
    owner = models.OneToOneField(User, verbose_name=_('Dono'))
    name = models.CharField(max_length=50, verbose_name=_('Nome'))
    short_name = models.CharField(
        max_length=4,
        verbose_name=_('Nome Abreviado'),
        help_text=_('Somente iniciais. Ex: Bruno Lucena = BL')
    )
    email = models.EmailField(max_length=200, verbose_name='Email')
    company_name = models.CharField(
        null=True,
        blank=True,
        max_length=100,
        verbose_name=_('Nome Empresarial')
    )
    address = models.CharField(
        null=True,
        blank=True,
        max_length=200,
        verbose_name=_('Endereço'),
        help_text=_('Endereço completo como deve aparecer no contrato')
    )
    cnpj = models.CharField(
        null=True,
        blank=True,
        max_length=20,
        verbose_name=_('CNPJ')
    )
    id_owner = models.CharField(
        null=True,
        blank=True,
        max_length=20,
        verbose_name=_('RG do Dono')
    )
    cpf_owner = models.CharField(
        null=True,
        blank=True,
        max_length=14,
        verbose_name=_('CPF do Dono')
    )
    logo = models.ImageField(
        null=True,
        blank=True,
        upload_to='main/store/logos',
        help_text='125x70px'
    )

    def __str__(self):
        return str(self.name)

    def __unicode__(self):
        return str(self.name)

    class Meta:
        verbose_name = _('Loja')
        verbose_name_plural = _('Lojas')

    def get_cc(self):
        '''Emails that must receive copies of emails from this store'''
        cc = self.emailrecipients_set.all()
        return cc

    def get_fields(self):
        '''Return a tuple with all fields'''
        return self._meta.fields

    def get_payment_methods(self):
        '''Payment methods accepted by this store'''
        payment_methods = self.paymentmethods_set.all()
        return payment_methods

    def get_persons(self):
        '''Persons from this store'''
        persons = self.person_set.all()
        return persons

    def get_phones(self):
        '''Phone numbers'''
        phones = self.storephones_set.all()
        return phones

    def get_services(self):
        '''Services for this store'''
        services = self.product_set.all()
        return services

    def get_services_not_required(self):
        '''Services not required'''
        services = self.get_services().filter(required=False)
        return services

    def get_services_not_required_and_active(self):
        '''Services not required and active'''
        services = self.get_services_not_required().filter(active=True)
        return services

    def get_services_required(self):
        '''Services required'''
        services = self.get_services().filter(required=True)
        return services

    def get_services_required_and_active(self):
        '''Services required and active'''
        services = self.get_services_required().filter(active=True)
        return services

    def get_users(self):
        '''Users from this store'''
        users = [person for person in self.get_persons()]
        return users


class StorePhones(models.Model):
    store = models.ForeignKey(
        Store,
        verbose_name=_('Loja')
    )
    phone = models.CharField(
        max_length=11,
        default='',
        null=True,
        blank=True,
        verbose_name='Telefone'
    )

    def __str__(self):
        return str(self.phone)

    def __unicode__(self):
        return str(self.phone)

    class Meta:
        verbose_name = _('Loja Telefone')
        verbose_name_plural = _('Loja Telefones')


class Contract(models.Model):
    store = models.OneToOneField(Store, verbose_name=_('Loja'))
    title = models.TextField(
        max_length=100,
        default='CONTRATO DE PRESTAÇÃO DE SERVIÇOS PARA O DIA DA NOIVA',
        verbose_name=_('Título')
    )
    header = models.TextField(max_length=5000, verbose_name=_('Cabeçalho'))
    footer = models.TextField(max_length=5000, verbose_name=_('Rodapé'))

    def __str__(self):
        return str(self.id)

    def __unicode__(self):
        return str(self.id)

    class Meta:
        verbose_name = _('Contrato')
        verbose_name_plural = _('Contratos')

    def get_articles(self):
        '''Articles'''
        articles = self.contractarticles_set.all()
        return articles

    def render_content(self, content='title', args={}):
        '''Takes a text and args, and replaces the tags on
        text with the args values.

        >>> replace_tags_for_values(
        >>>     text='test {{ var_test }} test2 {{ var_test2 }}',
        >>>     args={'var_test': value1, 'var_test2': value2}
        >>>     )
        >>> 'test value1 test2 value2'
        '''
        if content == 'title':
            text = self.title
        elif content == 'header':
            text = self.header
        elif content == 'footer':
            text = self.footer

        a = [m.start() for m in re.finditer('{{', text)]
        b = [m.start() for m in re.finditer('}}', text)]

        for i in range(len(a)):
            value1 = a[i]
            value2 = b[i]
            tag = text[value1:value2].replace('{{', '').replace(' ', '')
            for key, value in args.items():
                if key == tag:
                    text = text.replace(' ' + key + ' ', value)
            a = [m.start() for m in re.finditer('{{', text)]
            b = [m.start() for m in re.finditer('}}', text)]

        text = text.replace('{{', '').replace('}}', '')
        return text


class ContractArticles(models.Model):
    contract = models.ForeignKey(Contract, verbose_name=_('Contrato'))
    title = models.CharField(
        max_length=100,
        default='CLÁUSULA',
        verbose_name=_('Título')
    )
    content = models.TextField(max_length=5000, verbose_name=_('Conteúdo'))

    def __str__(self):
        return str(self.id)

    def __unicode__(self):
        return str(self.id)

    class Meta:
        verbose_name = _('Cláusula')
        verbose_name_plural = _('Cláusulas')
        ordering = ['id']

    def render_content(self, content='content', args={}):
        '''Takes a text and args, and replaces the tags on
        text with the args values.

        >>> replace_tags_for_values(
        >>>     text='test {{ var_test }} test2 {{ var_test2 }}',
        >>>     args={'var_test': value1, 'var_test2': value2}
        >>>     )
        >>> 'test value1 test2 value2'
        '''
        if content == 'content':
            text = self.content
        elif content == 'title':
            text = self.title

        a = [m.start() for m in re.finditer('{{', text)]
        b = [m.start() for m in re.finditer('}}', text)]

        for i in range(len(a)):
            value1 = a[i]
            value2 = b[i]
            tag = text[value1:value2].replace('{{', '').replace(' ', '')
            for key, value in args.items():
                if key == tag:
                    text = text.replace(' ' + key + ' ', value)
            a = [m.start() for m in re.finditer('{{', text)]
            b = [m.start() for m in re.finditer('}}', text)]

        text = text.replace('{{', '').replace('}}', '')
        return text


class Person(models.Model):
    user = models.OneToOneField(User)
    store = models.ForeignKey(
        Store,
        verbose_name=_('Loja')
    )
    role = models.CharField(
        max_length=100,
        null=True,
        blank=True,
        verbose_name='Cargo'
    )
    cpf = models.CharField(
        max_length=11,
        verbose_name='CPF',
        null=True,
        blank=True
    )
    address = models.CharField(
        max_length=200,
        default='',
        null=True,
        blank=True,
        verbose_name='Endereco'
    )
    number = models.CharField(
        max_length=200,
        default='',
        null=True,
        blank=True,
        verbose_name='Numero'
    )
    complement = models.CharField(
        max_length=200,
        default='',
        null=True,
        blank=True,
        verbose_name='Complemento'
    )
    district = models.CharField(
        max_length=200,
        default='',
        null=True,
        blank=True,
        verbose_name='Bairro'
    )
    ddd = models.CharField(
        max_length=2,
        default='',
        null=True,
        blank=True,
        verbose_name='DDD'
    )
    phone = models.CharField(
        max_length=11,
        default='',
        null=True,
        blank=True,
        verbose_name='Telefone'
    )
    city = models.CharField(
        max_length=50,
        default='',
        null=True,
        blank=True,
        verbose_name='Cidade'
    )
    state = models.CharField(
        max_length=2,
        default='',
        null=True,
        blank=True,
        verbose_name='Estado'
    )
    cep = models.CharField(
        max_length=8,
        default='',
        null=True,
        blank=True,
        verbose_name='CEP'
    )
    admin = models.BooleanField(
        default=False,
        verbose_name='Admin'
    )
    max_discount = models.DecimalField(
        max_digits=2,
        decimal_places=0,
        validators=[MaxValueValidator(99), MinValueValidator(0)],
        default=0,
        verbose_name='Desconto Máximo'
    )

    def __unicode__(self):
        return str(self.user)

    def __str__(self):
        return str(self.user)

    class Meta:
        verbose_name = 'Pessoa'
        verbose_name_plural = 'Pessoas'

    @staticmethod
    def create_user(email, password, username=None):
        if username is None:
            username = email
        u = User.objects.get_or_create(username=username, email=email)[0]
        return u

    def bank_accounts(self):
        '''Bank accounts from this person.
        '''
        bank_accounts = self.bankaccount_set.all()
        return bank_accounts

    def get_bank_accounts_div(self):
        '''Bank accounts in div format.
        '''
        bank_accounts = self.bank_accounts()
        html = ('<div class="col-sm-12"><p>')
        for bank_account in bank_accounts:
            html += '<div class="col-sm-4"><em>'
            html += bank_account.full_name + '<br/>'
            html += bank_account.bank + '<br/>'
            html += _('Agência ') + bank_account.agency + '<br/>'
            html += _('Conta ') + bank_account.number + '<br/>'
            html += _('CNPJ ou CPF ') + bank_account.doc_id + '<br/><br/><br/>'
            html += '</em></div>'
        html += '</p></div>'
        return html


class BankAccount(models.Model):
    person = models.ForeignKey(Person, verbose_name='Pessoa')
    full_name = models.CharField(max_length=200, verbose_name='Nome Completo')
    bank = models.CharField(max_length=100, verbose_name='Nome do Banco')
    agency = models.CharField(max_length=20, verbose_name='Agência')
    number = models.CharField(max_length=50, verbose_name='Número')
    doc_id = models.CharField(max_length=50, verbose_name='CPF ou CNPJ')

    def __unicode__(self):
        return str(self.id)

    def __str__(self):
        return str(self.id)

    class Meta:
        verbose_name = _('Conta Bancária')
        verbose_name_plural = _('Contas Bancárias')


class OrderNumber(models.Model):
    number = models.CharField(
        max_length=10,
        unique=True,
        verbose_name='Numero do Pedido'
    )

    def __str__(self):
        return str(self.number)

    def __unicode__(self):
        return str(self.number)

    class Meta:
        verbose_name = 'Pedido Numero'
        verbose_name_plural = 'Pedidos Numeros'


class Product(models.Model):
    name = models.CharField(
        max_length=200,
        verbose_name=_('Nome'),
        unique=False
    )
    default_price = models.DecimalField(
        max_digits=10,
        decimal_places=2,
        null=True,
        verbose_name=_('Preço Padrão (R$)'),
        help_text=_('Se não for encontrado um preço específico'
                    ' para o dia atual, este será utilizado')
    )
    store = models.ForeignKey(
        Store,
        verbose_name=_('Loja')
    )
    required = models.BooleanField(
        default=True,
        verbose_name=_('Obrigatório'),
        help_text=_('Serviço obrigatório')
    )
    active = models.BooleanField(
        default=True,
        verbose_name=_('Ativo'),
        help_text=_('Marque um serviço como inativo ao invés'
                    ' de excluí-lo')
    )

    def __unicode__(self):
        return str(self.name)

    def __str__(self):
        return str(self.name)

    class Meta:
        verbose_name = _('Serviço')
        verbose_name_plural = _('Serviços')
        unique_together = (('store', 'name'),)

    @property
    def price(self):
        '''Takes the current price based on the actual date
        or takes de default_price if there's none.'''
        product_prices = self.get_all_prices().filter(
            date_start__lte=now(),
            date_end__gte=now()
        )
        if product_prices.exists():
            price = product_prices.latest('price').price
        else:
            price = self.default_price
        return price

    def delete_price(self, price_id):
        '''Delete ProductPrice with id price_id'''
        product_price = self.get_price(price_id)
        product_price.delete()
        return True

    def get_price(self, price_id):
        '''Takes ProductPrice with id price_id'''
        product_price = ProductPrice.objects.get(id=price_id)
        return product_price

    def get_all_prices(self):
        '''Return all prices'''
        prices = self.productprice_set.all()
        return prices

    def add_price(self, price, date_start, date_end):
        '''Add ProductPrice'''
        price_object = round(float(price), 2)
        date_start_object = Order.str_to_date(date_start)
        date_end_object = Order.str_to_date(date_end)
        product_price = ProductPrice.objects.create(
            product=self,
            price=price_object,
            date_start=date_start_object,
            date_end=date_end_object
        )
        return product_price


class ProductPrice(models.Model):
    product = models.ForeignKey(Product, verbose_name='Serviço')
    price = models.DecimalField(
        max_digits=10,
        decimal_places=2,
        null=True,
        verbose_name='Preço (R$)'
    )
    date_start = models.DateField(verbose_name='Data Início')
    date_end = models.DateField(verbose_name='Data Fim')

    def __unicode__(self):
        return '{0} R${1}'.format(self.product.name, self.price)

    def __str__(self):
        return '{0} R${1}'.format(self.product.name, self.price)

    class Meta:
        verbose_name = 'Serviço Preco'
        verbose_name_plural = 'Serviços Precos'


class Order(models.Model):
    '''
    Order has all info about the customer, the service and a list
    of OrderDetail containing the services related to the order
    and it's price.
    Every Proposal is related to an Order and the details of the proposal
    are passed to the order when the customer closes the deal.

    Attributes defined here:
    store -- ForeignKey(Store)
    name -- Char
    lastname -- Char
    order_number -- OneToOne(OrderNumber)
    date_wedding -- Date
    time_arrival -- Time
    time_leaving -- Time
    street -- Char
    number -- Char
    complement -- Char
    district -- Char
    phone -- Char
    city -- Char
    state -- Char
    cep -- Char
    email -- Email
    cpf -- Char
    rg -- Char
    professional_hair -- Char
    professional_make_up -- Char
    date_test -- Date
    time_test -- Time
    payment_type -- Char
    installments -- Integer
    status -- Char (Choices)
    date_return -- Date
    date_closed -- Date
    discount -- Int -> Discout in percentage
    discount_total -- Float -> Discout to be applied
    total -- Float -> Total without discount applied
    total2 -- Float -> Total with discount applied
    proposal_deal -- Proposal -> Which proposal was deal with customer

    Static methods defined here:
    str_to_date() - Takes a str and return Date object
                    -> str, 'dd/mm/yyyy', 'yyyy/mm/dd' or 'mm/dd/yyyy'
    create_order_number() - New OrderNumber

    Public methods defined here:
    get_full_name -- str with formated name and lastname
    get_full_address() -- str with formated address
    get_details() -- OrderDetail
    get_proposals() -- Proposal
    get_historics() -- Historics
    get_products() -- Products in each OrderDetail
    get_all_fields() -- Dict with field.name as key and field.value as value
    get_installments() -- Installments
    clean_installments() -- Clean Installments
    set_installments(dict) -- Create OrderInstallments
    '''

    store = models.ForeignKey(
        Store,
        verbose_name=_('Loja')
    )
    name = models.CharField(
        max_length=20,
        verbose_name=_('Nome')
    )
    lastname = models.CharField(
        max_length=100,
        verbose_name='Sobrenome'
    )
    order_number = models.OneToOneField(
        OrderNumber,
        unique=True,
        null=True,
        verbose_name='Numero do Pedido'
    )
    date_wedding = models.DateField(
        verbose_name='Data do Casamento',
        help_text='Formato DD/MM/AAAA',
    )
    time_arrival = models.TimeField(
        auto_now=False,
        auto_now_add=False,
        null=True,
        blank=True,
        verbose_name='Horário de Entrada',
        help_text='Formato hh:mm'
    )
    time_leaving = models.TimeField(
        auto_now=False,
        auto_now_add=False,
        null=True,
        blank=True,
        verbose_name='Horário de Saida',
        help_text='Formato hh:mm'
    )
    street = models.CharField(
        max_length=200,
        default='',
        verbose_name='Rua'
    )
    number = models.CharField(
        max_length=200,
        default='',
        null=True,
        blank=True,
        verbose_name='Numero'
    )
    complement = models.CharField(
        max_length=200,
        default='',
        null=True,
        blank=True,
        verbose_name='Complemento'
    )
    district = models.CharField(
        max_length=200,
        default='',
        null=True,
        blank=True,
        verbose_name='Bairro'
    )
    phone = models.CharField(
        max_length=11,
        default='',
        verbose_name='Telefone'
    )
    city = models.CharField(
        max_length=50,
        default='',
        null=True,
        blank=True,
        verbose_name='Cidade'
    )
    state = models.CharField(
        max_length=50,
        default='',
        null=True,
        blank=True,
        verbose_name='Estado'
    )
    cep = models.CharField(
        max_length=8,
        default='',
        null=True,
        blank=True,
        verbose_name='CEP'
    )
    email = models.EmailField(
        max_length=200,
        verbose_name='Email'
    )
    cpf = models.CharField(
        max_length=50,
        default='',
        null=True,
        blank=True,
        verbose_name='CPF'
    )
    rg = models.CharField(
        max_length=50,
        default='',
        null=True,
        blank=True,
        verbose_name='RG'
    )
    professional_hair = models.CharField(
        max_length=100,
        null=True,
        blank=True,
        verbose_name='Profissional Cabelo'
    )
    professional_make_up = models.CharField(
        max_length=100,
        null=True,
        blank=True,
        verbose_name='Profissional Maquiagem'
    )
    date_test = models.DateField(
        null=True,
        blank=True,
        verbose_name='Data do Teste'
    )
    time_test = models.TimeField(
        auto_now=False,
        auto_now_add=False,
        null=True,
        blank=True,
        verbose_name='Horário do Teste',
        help_text='Formato hh:mm'
    )
    payment_type = models.CharField(
        max_length=100,
        null=True,
        blank=True,
        verbose_name='Forma de Pagamento'
    )
    installments = models.IntegerField(
        default=1,
        verbose_name='Quantidade de Parcelas'
    )
    status = models.CharField(
        max_length=50,
        choices=ORDER_STATUS,
        default='1',
        verbose_name='Status'
    )
    date_return = models.DateField(
        default=one_week,
        verbose_name='Data de Retorno',
        help_text='Data para retornar o contato com o cliente'
    )
    date_closed = models.DateField(
        null=True,
        blank=True,
        verbose_name='Data Finalizado'
    )
    date_created = models.DateField(
        default=now,
        verbose_name='Data'
    )

    def __unicode__(self):
        return str(self.id)

    def __str__(self):
        return str(self.id)

    class Meta:
        verbose_name = 'Cliente'
        verbose_name_plural = 'Clientes'

    def save(self, *args, **kwargs):
        if self.order_number is None or self.order_number == '':
            order_number = Order.create_order_number()
            self.order_number = order_number
            super(Order, self).save(*args, **kwargs)
        else:
            super(Order, self).save(*args, **kwargs)

    @property
    def discount(self):
        '''Discount of Proposal deal'''
        discount = None
        if self.proposal_deal:
            discount = self.proposal_deal.discount
        return discount

    @property
    def discount_total(self):
        '''Return float

        Calculate how mouch discount will be applied
        '''
        discount_total = None
        if self.proposal_deal:
            discount_total = self.proposal_deal.discount_total
        return discount_total

    @property
    def full_name(self):
        return '{name} {last_name}'.format(
            name=self.name,
            last_name=self.lastname
        )
    full_name.fget.short_description = 'Nome Completo'

    @property
    def total(self):
        '''Return float

        Total without discount
        '''
        total = None
        if self.proposal_deal:
            total = self.proposal_deal.total
        return total

    @property
    def total2(self):
        '''Return float

        Total minus discount
        '''
        discount_total = self.discount_total
        total = self.total
        total2 = None
        if total:
            total2 = round(float(total - discount_total), 2)
        return total2
    total2.fget.short_description = 'Total com descnto'

    @property
    def proposal_deal(self):
        '''Which proposal was accepted by the customer'''
        proposals = self.get_proposals()
        deal = None
        for proposal in proposals:
            if proposal.deal:
                deal = proposal
        return deal
    proposal_deal.fget.short_description = 'Proposta Fechada'

    @staticmethod
    def str_to_time(string_time):
        '''Takes a string formated time and return a
        datetime.time object

        string_time -> str, 'hh:mm'
        '''
        tm = string_time.split(':')
        if len(tm) != 2:
            raise SyntaxWarning('Invalid string_time')
        hour = int(tm[0])
        minute = int(tm[1])
        time = datetime.time(hour, minute)
        return time

    @staticmethod
    def str_to_date(string_date, date_format='dd/mm/yyyy'):
        '''Takes a string formated date and a date_format and
        return a datetime.date object.

        string_date -> str, separator can be '/' or '-'.
        date_format -> str, 'dd/mm/yyyy', 'yyyy/mm/dd' or 'mm/dd/yyyy'

        Raises:
            AttributeError
            IndexError
            SyntaxWarning
            TypeError
            ValueError
        '''
        dt = string_date.split('/')
        if len(dt) != 3:
            dt.split('-')
        if len(dt) != 3:
            raise SyntaxWarning('Invalid string_date')

        if date_format == 'dd/mm/yyyy':
            day = int(dt[0])
            month = int(dt[1])
            year = int(dt[2])
        elif date_format == 'yyyy/mm/dd':
            day = int(dt[2])
            month = int(dt[1])
            year = int(dt[0])
        elif date_format == 'mm/dd/yyyy':
            day = int(dt[1])
            month = int(dt[0])
            year = int(dt[2])
        else:
            raise SyntaxWarning('Invalid date_format')

        date = datetime.date(year, month, day)
        return date

    @staticmethod
    def create_order_number():
        order_number = None
        while order_number is None:
            try:
                n = create_order_number()
                try:
                    order_number = OrderNumber.objects.create(number=n)
                except Exception:
                    pass
            except Exception:
                pass
        return order_number

    def get_full_name(self):
        return '{name} {last_name}'.format(
            name=self.name,
            last_name=self.lastname
        )

    def get_full_address(self):
        '''Return str

        Formats the address and returns it
        '''
        return "{0} n{1}, {2}, Bairro {3}. Cidade {4}-{5}. CEP {6}".format(
            self.street,
            self.number,
            self.complement,
            self.district,
            self.city,
            self.state,
            self.cep
        )

    def get_products(self):
        '''Products list

        All Products related to self
        '''
        products = []
        if self.proposal_deal:
            for proposal_detail in self.proposal_deal:
                products.append(proposal_detail.product)
        return products

    def get_proposals(self):
        '''All Proposals'''
        proposals = self.proposal_set.all()
        return proposals

    def get_historics(self):
        '''All Historics'''
        historics = self.orderhistoric_set.all().order_by('-date')
        return historics

    def get_all_fields(self):
        '''Dict with all field names and its values'''
        obj = serializers.serialize('json', [self])
        json_obj = json.loads(obj)
        fields = json_obj[0]['fields']
        return fields

    def get_installments(self):
        '''All Installments'''
        installments = self.orderinstallments_set.all().order_by('-payment_due')
        return installments

    def clean_installments(self):
        '''Clean all installments'''
        for installment in self.get_installments():
            installment.delete()
        return True

    def set_installments(self, installments_dict={}):
        '''Create OrderInstallments for each object in dict'''
        self.clean_installments()
        for i in installments_dict.items():
            OrderInstallments.objects.create(
                order=self,
                total=round(float(i[1]['value']), 2),
                payment_due=self.str_to_date(i[1]['date'])
            )
        return True


class OrderInstallments(models.Model):
    order = models.ForeignKey(
        Order,
        verbose_name='Pedido'
    )
    total = models.DecimalField(
        max_digits=10,
        decimal_places=2,
        null=True,
        blank=True,
        verbose_name='Total',
        help_text='Valor da Parcela'
    )
    payment_due = models.DateField(
        null=True,
        blank=True,
        verbose_name='Vencimento Dia'
    )
    payment = models.BooleanField(
        default=False,
        verbose_name='Pago'
    )
    payment_date = models.DateField(
        null=True,
        blank=True,
        verbose_name='Pago Dia'
    )

    def __unicode__(self):
        return str(self.order)

    def __str__(self):
        return str(self.order)

    class Meta:
        verbose_name = 'Parcela'
        verbose_name_plural = 'Parcelas'


class PaymentMethodsSystem(models.Model):
    name = models.CharField(
        max_length=100,
        verbose_name='Forma de Pagamento'
    )
    type = models.CharField(
        max_length=100,
        choices=PAYMENT_METHOD_TYPE,
        verbose_name='Tipo'
    )
    description = models.CharField(
        max_length=200,
        null=True,
        blank=True,
        verbose_name='Descrição'
    )
    image = models.ImageField(
        upload_to="main/payment_methods",
        null=True,
        blank=True,
        verbose_name="Imagem"
    )

    def __unicode__(self):
        return str(self.name)

    def __str__(self):
        return str(self.name)

    class Meta:
        verbose_name = 'Forma de Pagamento no Sistema'
        verbose_name_plural = 'Formas de Pagamento no Sistema'


class PaymentMethods(models.Model):
    store = models.ForeignKey(
        Store,
        verbose_name='Loja'
    )
    payment_method = models.ForeignKey(
        PaymentMethodsSystem,
        verbose_name='Forma de Pagamento'
    )
    installments = models.IntegerField(
        default=1,
        verbose_name='Quantidade de Parcelas Permitidas'
    )

    def __unicode__(self):
        return str(self.payment_method)

    def __str__(self):
        return str(self.payment_method)

    class Meta:
        verbose_name = 'Forma de Pagamento'
        verbose_name_plural = 'Formas de Pagamento'


def get_superuser():
    try:
        person = Person.objects.get(user=User.objects.get(username='bruno'))
    except Exception:
        person = Person.objects.all()[0]
    return person


class Proposal(models.Model):
    order = models.ForeignKey(
        Order,
        verbose_name='Cliente'
    )
    deal = models.BooleanField(
        default=False,
        verbose_name='Escolhido Pelo Cliente'
    )
    date = models.DateTimeField(
        default=now,
        verbose_name='Data'
    )
    due_date = models.DateTimeField(
        default=one_month,
        verbose_name='Data de Vencimento'
    )
    attendant = models.ForeignKey(
        Person,
        on_delete=models.SET(get_superuser),
        verbose_name='Atendente'
    )
    discount = models.DecimalField(
        max_digits=2,
        decimal_places=0,
        null=True,
        blank=True,
        verbose_name='Desconto (%)'
    )
    proposal = models.FileField(
        null=True,
        blank=True,
        upload_to='propostas',
        verbose_name='Proposta Pdf'
    )
    proposal_sent = models.BooleanField(
        default=False,
        verbose_name='Proposta Enviada'
    )
    proposal_date_sent = models.DateField(
        null=True,
        blank=True,
        verbose_name='Data de Envio da Proposta'
    )
    contract = models.FileField(
        null=True,
        blank=True,
        upload_to='contratos',
        verbose_name='Contrato'
    )
    contract_sent = models.BooleanField(
        default=False,
        verbose_name='Contrato Enviado'
    )
    contract_date_sent = models.DateField(
        null=True,
        blank=True,
        verbose_name='Data de Envio do Contrato'
    )
    payment_method = models.CharField(
        null=True,
        blank=True,
        max_length=100,
        verbose_name='Forma de Pagamento'
    )

    class Meta:
        verbose_name = 'Proposta'
        verbose_name_plural = 'Propostas'

    def __unicode__(self):
        return '%s' % (self.id)

    def __str__(self):
        return '%s' % (self.id)

    @property
    def total(self):
        '''Return float

        Total without discount
        '''
        total = 0
        for detail in self.details():
            total += round(float(detail.price), 2)
        return total

    @property
    def total2(self):
        '''Return float

        Total minus discount
        '''
        discount_total = self.discount_total
        total = self.total
        total2 = round(float(total - discount_total), 2)
        return total2
    total2.fget.short_description = 'Total com descnto'

    @property
    def discount_total(self):
        '''Return float

        Calculate how mouch discount will be applied
        '''
        discount = self.discount
        total = self.total
        if discount is not None and discount != 0:
            discount_total = round(float(total * float(discount) / 100), 2)
        else:
            discount_total = 0
        return discount_total

    def details(self):
        '''Return ProposalDetail list.

        All ProposalDetail's related to self.
        '''
        details = self.proposaldetail_set.all()
        return details

    def get_services(self):
        '''Services list

        Services related to self
        '''
        services = []
        for proposal_detail in self.details():
            services.append(proposal_detail.service)
        return services

    def get_services_div(self):
        '''Services in div format.

        <div class="col-sm-2">{{ service }}</div>
        '''
        html = '<div class="col-sm-12"><p>'
        for service in self.get_services():
            html += '<div class="col-sm-2"><em>' + service.name + '</em></div><br/>'
        html += '</p></div>'
        return html

    def get_services_ul(self):
        '''Services in list format.

        <ul><li>{{ service }}</li></ul>
        '''
        html = '<ul>'
        for service in self.get_services():
            html += '<li>' + service.name + '</li>'
        html += '</ul>'
        return html

    def get_payment_conditions_text(self):
        '''Text explaining the payment conditions.
        '''
        installments = list(self.order.get_installments())
        installments_qt = self.order.installments
        last_installment = DateFormat(installments[-1].payment_due).format('d/m/Y')

        text = ''
        text += 'A quantia acima será paga em '
        text += str(installments_qt)
        if installments_qt == 1:
            text += ' parcela no valor de R$'
            text += ponto_para_virgula(format(installments[0].total, '.2f'))
            text += ', sendo paga até o dia '
            text += str(last_installment)
            text += '.'
        else:
            text += ' parcelas, conforme informações abaixo:<br/>'
            text += '<div class="col-sm-12"><p>'
            for installment in installments:
                payment_due = DateFormat(installment.payment_due).format('d/m/Y')
                total = ponto_para_virgula(format(installment.total, '.2f'))
                text += '<div class="col-sm-2"><em>' + 'R$' + total + ' - ' + payment_due + '</em></div>'
            text += '</p></div><br/><br/><br/><br/>'

        return text

    def services(self):
        '''Products in self.details'''
        services = []
        for detail in self.details():
            services.append(detail.service)
        return services

    def pdfy_api(self, request, type_pdf='proposal', testing=False):
        '''Create pdf from pdfy api.

        Return list with response and path to be associated with file.
        type_pdf canb be 'proposal' or 'contract' to specify if it's
        creating a proposal or a contract.
        '''
        try:
            os.makedirs(os.path.join(settings.MEDIA_ROOT, 'propostas'))
        except FileExistsError:
            if not os.path.isdir(
                os.path.join(settings.MEDIA_ROOT, 'propostas')
            ):
                raise
        try:
            os.makedirs(os.path.join(settings.MEDIA_ROOT, 'contratos'))
        except FileExistsError:
            if not os.path.isdir(
                os.path.join(settings.MEDIA_ROOT, 'contratos')
            ):
                raise

        self.request = request
        domain = request.META['HTTP_HOST']

        proposal = self
        order = self.order

        if not testing:
            token = TokenAccess.objects.create()

            if type_pdf == 'proposal':
                path = ('/proposta/' + str(proposal.id) + '/gerar_pdf/' + token.token.decode('utf-8') + '/')
                page = ('http://' + str(domain) + path)
                token.page = path
                token.save()
                counter = 0
                name = (
                    'propostas/Cliente' +
                    str(order.id) +
                    '_' +
                    'Proposta' +
                    str(proposal.id) +
                    '.pdf'
                )
                path = os.path.join(settings.MEDIA_ROOT, name)
                while os.path.exists(path):
                    counter = counter + 1
                    name = (
                        'propostas/Cliente' +
                        str(order.id) +
                        '_' +
                        'Proposta' +
                        str(proposal.id) +
                        '_' +
                        str(counter) +
                        '.pdf'
                    )
                    path = os.path.join(settings.MEDIA_ROOT, name)
            elif type_pdf == 'contract':
                path = ('/contrato/' + str(proposal.id) + '/gerar_pdf/' + token.token.decode('utf-8') + '/')
                page = ('http://' + str(domain) + path)
                token.page = path
                token.save()
                counter = 0
                name = (
                    'contratos/Cliente' +
                    str(order.id) +
                    '_' +
                    'Contrato' +
                    str(proposal.id) +
                    '.pdf'
                )
                path = os.path.join(settings.MEDIA_ROOT, name)
                while os.path.exists(path):
                    counter = counter + 1
                    name = (
                        'contratos/Cliente' +
                        str(order.id) +
                        '_' +
                        'Contrato' +
                        str(proposal.id) +
                        '_' +
                        str(counter) +
                        '.pdf'
                    )
                    path = os.path.join(settings.MEDIA_ROOT, name)
            else:
                raise AttributeError('Invalid type_pdf')
        else:
            page = 'http://www.google.com.br'
            counter = 0
            name = (
                'diversos/{filename}.{format}'
                .format(
                    filename=get_random_string(length=12),
                    format='pdf'
                )
            )
            path = os.path.join(settings.MEDIA_ROOT, name)
            while os.path.exists(path):
                counter = counter + 1
                name = (
                    'diversos/{filename}.{format}'
                    .format(
                        filename=get_random_string(length=12),
                        format='pdf'
                    )
                )
                path = os.path.join(settings.MEDIA_ROOT, name)

        url_to_api = 'http://pdfy.net/api/GeneratePDF.aspx?url=%s' % (page)

        response = requests.get(url_to_api)
        response.raise_for_status()

        return [response, name]

    def create_pdf_from_page(self, request, type_pdf='proposal', testing=False):
        '''Create pdf from web page and returns its full path.

        type_pdf canb be 'proposal' or 'contract' to specify if it's
        creating a proposal or a contract.
        '''

        pdfy_api = self.pdfy_api(request, type_pdf=type_pdf, testing=testing)
        response = pdfy_api[0]
        name = pdfy_api[1]

        myFile = ContentFile(response.content)
        myFile.name = name

        fs = FileSystemStorage()
        filename = fs.save(myFile.name, myFile)

        return filename

    def create_pdf(self, request, type_pdf='proposal', testing=False):
        '''Create pdf.'''
        if type_pdf == 'proposal':
            if self.proposal == '' or self.proposal is None:
                file_path = self.create_pdf_from_page(
                    request=request,
                    type_pdf=type_pdf,
                    testing=testing
                )
                self.proposal = file_path
                self.save()

        elif type_pdf == 'contract':
            if self.contract == '' or self.contract is None:
                file_path = self.create_pdf_from_page(
                    request=request,
                    type_pdf=type_pdf,
                    testing=testing
                )
                self.contract = file_path
                self.save()

        return True

    def send_proposal(self,
                      request,
                      date_return,
                      testing=False,
                      subject='Proposta Dia da Noiva',
                      sender='diadanoiva@bruno-lucena.com.br',
                      type_pdf='proposal'):
        '''Sends proposal.pdf to customer email. '''
        if self.proposal == '' or self.proposal is None:
            self.create_pdf(
                request=request,
                type_pdf=type_pdf,
                testing=testing
            )

        self.proposal_date_sent = now()
        self.proposal_sent = True
        self.status = '0'  # enviado
        self.save()
        self.order.date_return = date_return
        self.order.save()

        OrderHistoric.objects.create(
            order=self.order,
            proposal=self,
            subject='Proposta enviada',
            description='Proposta enviada para o email do cliente',
            type='0'  # proposta
        )

        subject = subject
        to = self.order.email
        text_content = '''
            Olá {customer}.
            Segue em anexo proposta para dia da noiva do salão Lellit.
            Caso possua alguma dúvida, estamos
            a disposição para quaisquer contatos.


            Atenciosamente,
            {attendant} - Lellit
            {sender}
            '''.format(
            customer=self.order.name,
            attendant=self.attendant,
            sender=sender
        )
        params = {
            'attendant': self.attendant,
            'customer': self.order.name,
            'proposal': self.id,
            'sender_email': sender,
            'store': self.order.store
        }
        html_content = render_to_string('main/proposal_email.html', params)
        cc = self.order.store.get_cc()

        if not testing:
            msg = EmailMultiAlternatives(subject, text_content, sender, [to], cc=cc)
            msg.attach_alternative(html_content, "text/html")
            msg.attach_file(self.proposal.path)
            try:
                msg.send(fail_silently=False)
            except SMTPException as e:
                print('There was an error sending an email: ', e)
                return False

        return True

    def send_contract(self,
                      request,
                      date_return,
                      testing=False,
                      subject='Proposta Dia da Noiva',
                      sender='diadanoiva@bruno-lucena.com.br',
                      type_pdf='contract'):
        '''Sends contract.pdf to customer email.'''
        if self.contract == '' or self.contract is None:
            self.create_pdf(request=request, type_pdf=type_pdf, testing=testing)

        self.contract_date_sent = now()
        self.contract_sent = True
        self.status = '0'  # enviado
        self.save()
        self.order.date_return = date_return
        self.order.save()

        OrderHistoric.objects.create(
            order=self.order,
            proposal=self,
            subject='Contrato enviado',
            description='Contrato enviado para o email do cliente',
            type='0'  # proposta
        )

        subject = subject
        to = self.order.email
        text_content = '''
            Olá {customer}.
            Segue em anexo contrato para dia da noiva do salão {{ store }}.
            Caso possua alguma dúvida, estamos a
            disposição para contato.


            Atenciosamente,
            {attendant} - {{ store }}
            {sender}
            '''.format(
            customer=self.order.name,
            attendant=self.attendant,
            sender=sender
        )
        params = {
            'attendant': self.attendant,
            'customer': self.order.name,
            'proposal': self.id,
            'sender_email': sender,
            'store': self.order.store
        }
        html_content = render_to_string('main/contract_email.html', params)
        cc = self.order.store.get_cc()

        if not testing:
            msg = EmailMultiAlternatives(
                subject,
                text_content,
                sender,
                [to],
                cc=cc
            )
            msg.attach_alternative(html_content, "text/html")
            msg.attach_file(self.contract.path)
            try:
                msg.send(fail_silently=False)
            except SMTPException as e:
                print('There was an error sending an email: ', e)
                return False

        return True


class ProposalDetail(models.Model):
    proposal = models.ForeignKey(
        Proposal,
        verbose_name='Proposta'
    )
    service = models.ForeignKey(
        Product,
        verbose_name='Servico',
        on_delete=models.SET_NULL, null=True
    )

    class Meta:
        verbose_name_plural = 'Propostas Detalhes'

    def __unicode__(self):
        return '%s' % (self.proposal)

    def __str__(self):
        return '%s' % (self.proposal)

    @property
    def price(self):
        '''Price of service'''
        price = self.service.price
        return price


class TokenAccess(models.Model):
    token = models.CharField(
        max_length=300,
        unique=True,
        default=create_hash,
        verbose_name='Token'
    )
    due_time = models.DateTimeField(
        verbose_name='Horário de expiração',
        default=five_minutes_from_now
    )
    page = models.CharField(
        max_length=300,
        null=True,
        blank=True,
        verbose_name='Página de Acesso'
    )

    def __str__(self):
        return str(self.token)

    def __unicode__(self):
        return str(self.token)

    class Meta:
        verbose_name = 'Token API PDF'
        verbose_name_plural = 'Tokens API PDF'

    def is_expired(self):
        '''Check if token is expired.
        Token expires in five minutes.
        '''
        check = self.due_time < now()

        return check

    def same_page(self, page):
        '''Check if token page is the same
        as the one trying to access.
        '''
        check = self.page == page

        return check


class OrderHistoric(models.Model):
    order = models.ForeignKey(
        Order,
        verbose_name='Pedido'
    )
    proposal = models.ForeignKey(
        Proposal,
        null=True,
        blank=True,
        verbose_name='Proposta'
    )
    subject = models.CharField(
        max_length=100,
        verbose_name='Assunto'
    )
    date = models.DateTimeField(
        default=now,
        verbose_name='Data'
    )
    date_return = models.DateField(
        default=one_week,
        verbose_name='Data de Retorno'
    )
    status = models.CharField(
        max_length=50,
        choices=ORDER_STATUS,
        default='0',
        verbose_name='Status'
    )
    description = models.TextField(
        max_length=5000,
        verbose_name='Descricao'
    )
    type = models.CharField(
        max_length=50,
        choices=HISTORIC_TYPE,
        default='0',
        verbose_name='Tipo'
    )

    class Meta:
        verbose_name_plural = 'Pedidos Historico'

    def __unicode__(self):
        return '%s' % (self.order)

    def __str__(self):
        return '%s' % (self.order)


class EmailRecipients(models.Model):
    store = models.ForeignKey(
        Store,
        verbose_name=_('Loja')
    )
    email = models.EmailField(
        unique=True,
        max_length=200,
        verbose_name='Email')
    main = models.BooleanField(
        default=False,
        verbose_name='Principal',
        help_text=_('Email principal será utilizado para assinar os emails enviados por esta loja. '
                    'Somente um pode ser utilizado.')
    )

    def __str__(self):
        return str(self.store)

    def __unicode__(self):
        return str(self.store)

    class Meta:
        verbose_name = _('Recipiente de Email')
        verbose_name_plural = 'Recipientes de Email'
