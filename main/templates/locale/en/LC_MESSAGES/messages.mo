��          �      <      �  I   �     �          )     2  7   K     �      �     �     �  !   �     �     �       	        $     9  P  K  F   �     �     �  	          >   &     e     {     �     �     �  	   �     �  	   �  	   �     �     �                      
                                         	                                      
          Crie um usuário e tenha acesso à versão de testes.
         Cadastro Rápido Campo atualizado com sucesso Cliente  Criar Usuário de Testes Deseja mesmo enviar a proposta para o email do cliente? Esqueceu a senha? Experimente a versão de testes! Início Log in Logue-se para começar a utilizar MENU PRINCIPAL Membro desde %(date)s  Pesquisar... Proposta  Voltar Para Proposta date_return_error Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2017-09-30 15:12-0300
PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE
Last-Translator: Bruno A. Lucena <bruno.lucenac@gmail.com>
Language-Team: LANGUAGE <LL@li.org>
Language: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
 
          Create a user and gain access to the test version.
         New Customer Field updated successfully Customer  Create test user Do you really want to send the proposal to the customer email? Forgot your password? Test trial! Home Log in Login to start using MAIN MENU Member since %(date)s Search... Proposal  Go back to proposal date_return_error 