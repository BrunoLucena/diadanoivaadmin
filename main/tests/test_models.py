import os
import datetime
from pathlib import Path

from django.core import mail
from django.core.exceptions import ObjectDoesNotExist
from django.core.urlresolvers import reverse
from django.test import TestCase, RequestFactory
from django.utils import timezone

from main.models import *
from gettingstarted.settings import MEDIA_ROOT


def now_default_tz():
    return timezone.make_aware(datetime.datetime.now(), timezone.get_default_timezone())


def one_month():
    return now() + datetime.timedelta(days=30)


class UserCase(TestCase):
    '''User class'''

    def setUp(self):
        self.user = UserCase.get_user(self)
        self.store = StoreCase.get_store(self)
        self.Person = PersonCase.get_person(self)

    def get_user(self):
        try:
            user = User.objects.get(username='Bruno')
        except ObjectDoesNotExist:
            user = User.objects.create_user('Bruno')
            user.set_password('123456')
            user.first_name = 'Bruno'
            user.last_name = 'Lucena'
            user.email = 'bruno.lucenac@gmail.com'
            user.save()

        return user

    def test_create_user(self):
        '''Should create new User.'''
        user = UserCase.get_user(self)

        actual = user.username
        expected = 'Bruno'

        self.assertTrue(isinstance(user, User))
        self.assertEqual(
            actual, expected,
            'User.objects.create_user() should correctly create a user'
        )


class StoreCase(TestCase):
    '''Store class tests'''

    def setUp(self):
        self.user = UserCase.get_user(self)
        self.store = StoreCase.get_store(self)

    def get_store(self):
        try:
            store = Store.objects.get(
                name='Dia da Noiva'
            )
        except ObjectDoesNotExist:
            store = Store.objects.create(
                owner=self.user,
                name='Dia da Noiva',
                email='bruno.lucenac@gmail.com'
            )

        return store

    def test_create_store(self):
        '''Should create new store.'''
        store = StoreCase.get_store(self)

        actual = store.name
        expected = 'Dia da Noiva'

        self.assertTrue(isinstance(store, Store))
        self.assertEqual(
            actual, expected,
            'Store.objects.create() should correctly create a store'
        )
        self.assertEqual(
            store.__unicode__(), store.name,
            'Store.__unicode__ should be the same as Store.name'
        )
        self.assertEqual(
            store.__str__(), store.name,
            'Store.__str__ should be the same as Store.name'
        )

    def test_get_cc(self):
        '''Should return list of EmailRecipients from this store.'''
        store = self.store
        er = EmailRecipients.objects.create(
            store=store,
            email='bruno.lucenac@gmail.com'
        )
        er2 = EmailRecipients.objects.create(
            store=store,
            email='bruno.lucenac2@gmail.com'
        )
        cc = store.get_cc()

        actual = list(cc)
        expected = [er, er2]

        self.assertEqual(
            actual, expected,
            'Store.get_cc() should return a list of EmailRecipients'
        )

    def test_get_payment_methods(self):
        '''Should return list of PaymentMethods from this store'''
        store = self.store
        pm = PaymentMethods.objects.create(
            store=store,
            payment_method=PaymentMethodsSystemCase.create_payment_method_system(self)
        )

        payment_methods = store.get_payment_methods()

        actual = list(payment_methods)
        expected = [pm]

        self.assertEqual(
            actual, expected,
            ('Store.get_payment_methods() '
                'should return a list of PaymentMethods')
        )


class PersonCase(TestCase):
    ''' Person '''

    def setUp(self):
        self.user = UserCase.get_user(self)
        self.store = StoreCase.get_store(self)

    def get_person(self):
        try:
            person = Person.objects.get(
                user=UserCase.get_user(self)
            )
        except ObjectDoesNotExist:
            person = Person.objects.create(
                user=UserCase.get_user(self),
                store=StoreCase.get_store(self)
            )

        person.admin = True
        person.save()

        return person

    def static_create_user(self):
        return Person.create_user(
            email='Leticia',
            password='123456'
        )

    def test_create_person(self):
        '''Should create new Person.'''
        person = PersonCase.get_person(self)

        actual = person.user.username
        expected = 'Bruno'

        self.assertTrue(isinstance(person, Person))
        self.assertEqual(
            actual, expected,
            'Person.objects.create() should correctly create a person.'
        )
        self.assertEqual(
            person.admin, True,
            'Person should be admin.'
        )
        self.assertEqual(
            person.__unicode__(), person.user.username,
            'Store.__unicode__ should be the same as Store.name.'
        )
        self.assertEqual(
            person.__str__(), person.user.username,
            'Store.__str__ should be the same as Store.name.'
        )

    def test_static_create_user(self):
        '''Should create new User.'''
        user = self.static_create_user()
        actual = user.username
        expected = 'Leticia'

        self.assertTrue(isinstance(user, User))
        self.assertEqual(
            actual, expected,
            ('Person.create_user(email, password, ?username) '
                'should create a User')
        )


class PaymentMethodsSystemCase(TestCase):
    def setUp(self):
        self.payment_method_system = self.create_payment_method_system()

    def create_payment_method_system(self):
        return PaymentMethodsSystem.objects.create(
            name='Mastercard',
            image='mastercard.jpg',
            type='1'
        )

    def test_create_payment_method_system(self):
        '''Should create new PaymentMethod'''
        payment_method_system = self.payment_method_system
        actual = self.payment_method_system.name
        expected = 'Mastercard'

        self.assertTrue(
            isinstance(
                payment_method_system, PaymentMethodsSystem
            )
        )
        self.assertEqual(
            actual, expected,
            ('PaymentMethodsSystem.create() '
                ' should create a new PaymentMethodsSystem')
        )
        self.assertEqual(
            payment_method_system.__unicode__(), payment_method_system.name,
            'Store.__unicode__ should be the same as Store.name'
        )
        self.assertEqual(
            payment_method_system.__str__(), payment_method_system.name,
            'Store.__str__ should be the same as Store.name'
        )


class PaymentMethodsCase(TestCase):
    def setUp(self):
        self.user = UserCase.get_user(self)
        self.store = StoreCase.get_store(self)
        self.payment_method_system = PaymentMethodsSystemCase.create_payment_method_system(self)
        self.payment_method = self.create_payment_method()

    def create_payment_method(self):
        return PaymentMethods.objects.create(
            store=self.store,
            payment_method=self.payment_method_system
        )

    def test_create_payment_method(self):
        '''Should create a PaymentsMethods'''
        payment_method = self.create_payment_method()

        actual = payment_method.payment_method.name
        expected = 'Mastercard'

        self.assertTrue(isinstance(payment_method, PaymentMethods))
        self.assertEqual(
            actual, expected,
            'PaymentMethods.create() should create a new PaymentMethods'
        )
        self.assertEqual(
            payment_method.__unicode__(), payment_method.payment_method.name,
            'Store.__unicode__ should be the same as Store.name'
        )
        self.assertEqual(
            payment_method.__str__(), payment_method.payment_method.name,
            'Store.__str__ should be the same as Store.name'
        )


class ProposalCase(TestCase):
    '''Proposal'''

    def setUp(self):
        self.user = UserCase.get_user(self)
        self.person = PersonCase.get_person(self)
        self.store = StoreCase.get_store(self)
        self.proposal = ProposalCase.get_proposal(self)
        self.factory = RequestFactory()

    def get_proposal(self):
        try:
            order_number = OrderNumber.objects.get(number='1234567890')
            order = Order.objects.get(order_number=order_number)
        except ObjectDoesNotExist:
            order_number = OrderNumber.objects.create(
                number='1234567890'
            )
            order = Order.objects.create(
                order_number=order_number,
                store=self.store,
                name='Bruno',
                lastname='Lucena',
                email='bruno.lucenac@gmail.com',
                date_wedding=datetime.date(2018, 5, 26)
            )

        try:
            proposal = Proposal.objects.get(
                order=order
            )
        except ObjectDoesNotExist:
            proposal = Proposal.objects.create(
                order=order,
                date=datetime.date.today(),
                attendant=self.person
            )

        return proposal

    def test_create_proposal(self):
        '''Should create a new Proposal'''
        proposal = ProposalCase.get_proposal(self)

        actual = proposal.order.order_number.number
        expected = '1234567890'

        self.assertEqual(
            actual, expected,
            'Proposal.order.order_number should be correct.'
        )
        self.assertEqual(
            proposal.date.date(), now_default_tz().date(),
            'Proposal date should be today.'
        )
        self.assertEqual(
            proposal.due_date.date(), one_month().date(),
            'Proposal due date should be one month from now.'
        )
        self.assertEqual(
            proposal.__unicode__(), str(proposal.id),
            'Store.__unicode__ should be the same as Store.name.'
        )
        self.assertEqual(
            proposal.__str__(), str(proposal.id),
            'Store.__str__ should be the same as Store.name.'
        )

    def test_pdfy_api(self):
        '''Should use pdfy api and return list with
        response and filename.
        '''
        url = reverse('home')
        request = self.factory.get(
            url,
            HTTP_HOST='docs.djangoproject.dev:8000'
        )

        proposal = ProposalCase.get_proposal(self)
        pdfy_api = proposal.pdfy_api(
            request,
            type_pdf='proposal',
            testing='True'
        )

        response = pdfy_api[0]
        name = pdfy_api[1]

        actual = response.status_code
        expected = 200

        self.assertEqual(
            actual, expected,
            'Should return 200 status_code.'
        )

        inside = name
        what = 'diversos/'

        self.assertIn(
            what, inside,
            ('Should have a path to be assigned to '
                'a new file')
        )

    def test_create_pdf_from_page(self):
        '''Should create a pdf from a specific url'''
        url = reverse('home')
        request = self.factory.get(
            url,
            HTTP_HOST='docs.djangoproject.dev:8000'
        )

        proposal = ProposalCase.get_proposal(self)

        file_path = proposal.create_pdf_from_page(request, testing=True)
        file = Path(os.path.join(MEDIA_ROOT, file_path))

        actual = file

        self.assertTrue(
            actual.exists(),
            'Proposal file testing=True should exist.'
        )

        file_path2 = proposal.create_pdf_from_page(request)
        file2 = Path(os.path.join(MEDIA_ROOT, file_path2))

        actual2 = file2

        self.assertTrue(
            actual2.exists(),
            'Proposal file testing=False should exist.'
        )

        file_path3 = proposal.create_pdf_from_page(
            request,
            type_pdf='contract'
        )
        file3 = Path(os.path.join(MEDIA_ROOT, file_path3))

        actual3 = file3

        self.assertTrue(
            actual3.exists(),
            'Contract file testing=False should exist.'
        )

    def test_send_proposal(self):
        '''Should create and send proposal to customer email'''
        url = reverse('home')
        request = self.factory.get(
            url,
            HTTP_HOST='docs.djangoproject.dev:8000'
        )

        date_return = datetime.date.today()

        proposal = ProposalCase.get_proposal(self)
        response = proposal.send_proposal(
            request,
            date_return,
            testing='False'
        )

        actual = response
        expected = True

        self.assertEqual(
            actual, expected,
            'Should return True if email is successfully sent.'
        )

    def test_send_contract(self):
        '''Should create and send contract to customer email'''
        url = reverse('home')
        request = self.factory.get(
            url,
            HTTP_HOST='docs.djangoproject.dev:8000'
        )

        date_return = datetime.date.today()

        proposal = ProposalCase.get_proposal(self)
        response = proposal.send_contract(
            request,
            date_return,
            testing='False'
        )

        actual = response
        expected = True

        self.assertEqual(
            actual, expected,
            'Should return True if email is successfully sent.'
        )

    def test_email_service(self):
        '''Should successfully send an email'''
        mail.send_mail(
            'Subject here', 'Here is the message.',
            'from@example.com', ['bruno.lucenac@gmail.com'],
            fail_silently=False,
        )

        # Test that one message has been sent.
        self.assertEqual(len(mail.outbox), 1)

        # Verify that the subject of the first message is correct.
        self.assertEqual(mail.outbox[0].subject, 'Subject here')
