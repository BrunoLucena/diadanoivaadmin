from django.test import TestCase

from main.functions import *


class FunctionsCase(TestCase):
    def test_underscores_to_spaces(self):
        '''Should replace underscores to spaces.
        '''
        text = 'lorem_ipsum_dolor'

        actual = underscores_to_spaces(text)
        expected = 'lorem ipsum dolor'

        self.assertEqual(
            actual, expected,
            'Should return a new string replacing underscores to espaces'
        )

    def test_underscores_to_scores(self):
        '''Should replace underscores to spaces.
        '''
        text = 'lorem_ipsum_dolor'

        actual = underscores_to_scores(text)
        expected = 'lorem-ipsum-dolor'

        self.assertEqual(
            actual, expected,
            'Should return a new string replacing underscores to scores'
        )

    def test_create_order_number(self):
        '''Should create a string consisting of 10 numbers.
        '''
        string = create_order_number()

        actual = len(string)
        expected = 10

        self.assertTrue(isinstance(int(string), int))
        self.assertEqual(
            actual, expected,
            'Should return a string with 10 numbers'
        )

    def test_virgula_para_ponto(self):
        '''Should replace commas to dots.
        '''
        text = '24,10'

        actual = virgula_para_ponto(text)
        expected = '24.1'

        self.assertTrue(isinstance(actual, str))
        self.assertEqual(
            actual, expected,
            'Should return a string replacing commas with dots'
        )

    def test_ponto_para_virgula(self):
        '''Should replace dots to commas.
        '''
        text = '24.10'

        actual = ponto_para_virgula(text)
        expected = '24,10'

        self.assertTrue(isinstance(actual, str))
        self.assertEqual(
            actual, expected,
            'Should return a string replacing dots with commas'
        )
