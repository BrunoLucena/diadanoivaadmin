import datetime
import unittest

from django.core.exceptions import ObjectDoesNotExist
from django.core.urlresolvers import reverse
from django.test import TestCase

from main.models import (
    User, Order, OrderNumber, Person, Proposal, Product, Store
)
from main.tests.test_models import (
    UserCase, StoreCase, PersonCase, ProposalCase
)


class ViewsCase(TestCase):
    '''Views'''

    def setUp(self):
        self.user = UserCase.get_user(self)
        self.store = StoreCase.get_store(self)
        self.person = PersonCase.get_person(self)
        self.proposal = ProposalCase.get_proposal(self)
        self.order = self.proposal.order

        try:
            self.user2 = User.objects.get(username='Test')
        except ObjectDoesNotExist:
            self.user2 = User.objects.create_user('Test')
            self.user2.set_password('123456')
            self.user2.first_name = 'Test Name'
            self.user2.last_name = 'Test Last Name'
            self.user2.email = 'test@test.com.br'
            self.user2.save()
        try:
            self.store2 = Store.objects.get(
                name='Test Store'
            )
        except ObjectDoesNotExist:
            self.store2 = Store.objects.create(
                owner=self.user2,
                name='Test Store',
                email='teststore@test.com'
            )
        try:
            self.person2 = Person.objects.get(
                user=self.user2
            )
        except ObjectDoesNotExist:
            self.person2 = Person.objects.create(
                user=self.user2,
                store=self.store2
            )

        try:
            order_number2 = OrderNumber.objects.get(number='0987654321')
            order2 = Order.objects.get(order_number=order_number2)
        except ObjectDoesNotExist:
            order_number2 = OrderNumber.objects.create(
                number='0987654321'
            )
            order2 = Order.objects.create(
                order_number=order_number2,
                store=self.store2,
                name='Cliente 2',
                lastname='Sobrenome2',
                email='bruno.lucenac@gmail.com',
                date_wedding=datetime.date(2018, 5, 26)
            )

        try:
            proposal2 = Proposal.objects.get(
                order=order2
            )
        except ObjectDoesNotExist:
            proposal2 = Proposal.objects.create(
                order=order2,
                date=datetime.date.today(),
                attendant=self.person
            )

        self.order2 = order2
        self.proposal2 = proposal2

        try:
            service = Product.objects.get(
                name='Produto Teste',
                store=self.store
            )
        except ObjectDoesNotExist:
            service = Product.objects.create(
                name='Produto Teste',
                default_price=99,
                store=self.store
            )

        try:
            service2 = Product.objects.get(
                name='Produto Teste 2',
                store=self.store2
            )
        except ObjectDoesNotExist:
            service2 = Product.objects.create(
                name='Produto Teste 2',
                default_price=88,
                store=self.store2
            )

        self.service = service
        self.service2 = service2

    def test_login(self):
        '''Should login a user'''
        url = reverse('login')
        args = {
            'username': 'Bruno',
            'password': '123456'
        }
        response = self.client.post(url, args)

        actual = response.status_code
        expected = 302

        self.assertEqual(
            actual, expected,
            'Login should return 302 status, redirect to home page.'
        )
        self.assertEqual(
            response.url, '/accounts/profile/',
            'Login should return 302 status, redirect to home page.'
        )

    def test_access_home(self):
        '''Should have User.username on page content'''
        self.test_login()

        user = self.user
        url = reverse('home')
        response = self.client.get(url)

        actual = response.status_code
        expected = 200

        self.assertEqual(
            actual, expected,
            'Login should return 200 status after is logged in.'
        )
        self.assertIn(
            user.username, str(response.content),
            'User.username must be in html content after login.'
        )

    def test_home_search(self):
        '''Search in home must return new page with results'''
        self.test_login()

        search = 'teste'
        url = reverse('home')

        response = self.client.get(url, {'search': search})

        actual = response.status_code
        expected = 200

        self.assertEqual(
            actual, expected,
            'Search form on home page should work.'
        )

    def test_access_usuarios(self):
        '''Should test all possibilites of accessing this page.'''
        self.test_login()

        person = self.person
        person.admin = False
        person.save()

        url = reverse('usuarios')
        response = self.client.get(url)

        actual = response.status_code
        expected = 302

        self.assertEqual(
            actual, expected,
            ('Should not allow a non admin user to access '
                'an admin page.')
        )
        self.assertEqual(
            response.url, '/not_authorized/',
            'Should redirect to "/not_authorized/" if user is not admin'
        )

        person.admin = True
        person.save()

    def test_access_authorization(self):
        '''Should test access of non admin and admin users.'''
        self.test_login()

        person = self.person
        person.admin = False
        person.save()

        user = self.user
        user2 = self.user2
        url = reverse('editar_usuario', args=[user.id])
        response = self.client.get(url)

        actual = response.status_code
        expected = 302

        self.assertEqual(
            actual, expected,
            ('Should not allow a non admin user to access '
                'an admin page.')
        )
        self.assertEqual(
            response.url, '/not_authorized/',
            'Should redirect to "/not_authorized/" if user is not admin'
        )

        user.person.admin = True
        user.person.save()
        response = self.client.get(url)

        actual = response.status_code
        expected = 200

        self.assertEqual(
            actual, expected,
            ('Should allow an admin user to access '
                'a user from the same store.')
        )
        self.assertIn(
            user.username, str(response.content),
            'Should have user_edit.username on page content.'
        )

        url = reverse('editar_usuario', args=[user2.id])
        response = self.client.get(url)

        actual = response.status_code
        expected = 302

        self.assertEqual(
            actual, expected,
            ('Should not allow a user to access '
                'a user from another store.')
        )

    def test_access_users(self):
        '''Should test access of users page.'''
        self.test_login()

        person = self.person
        person.admin = False
        person.save()

        url = reverse('usuarios')
        response = self.client.get(url)

        actual = response.status_code
        expected = 302

        self.assertEqual(
            actual, expected,
            ('Should not allow a non admin user to access '
                'an admin page.')
        )
        self.assertEqual(
            response.url, '/not_authorized/',
            'Should redirect to "/not_authorized/" if user is not admin'
        )

        person.admin = True
        person.save()

    def test_access_order(self):
        '''Should test access of order page.'''
        self.test_login()

        order = self.order
        order2 = self.order2

        url = reverse(
            'cliente',
            args=[
                order.id
            ]
        )

        response = self.client.get(url)

        content = str(response.content)
        expected = str('Cliente 1')

        self.assertIn(
            expected, content,
            ('Should allow a user to access '
                'a order from same store.')
        )

        url2 = reverse(
            'cliente',
            args=[
                order2.id
            ]
        )

        response2 = self.client.get(url2)
        try:
            actual = response2.url
        except AttributeError:
            actual = response2
        expected = '/not_authorized/'

        self.assertEqual(
            actual, expected,
            ('Should not allow a user to access '
                'a order from another store.')
        )

    def test_access_proposal(self):
        '''Should test access of proposal page.'''
        self.test_login()

        proposal = self.proposal
        order = self.order
        proposal2 = self.proposal2
        order2 = self.order2

        url = reverse(
            'proposta_detalhes',
            args=[
                order.id,
                proposal.id
            ]
        )

        response = self.client.get(url)

        content = str(response.content)
        expected = str('Proposta 1')

        self.assertIn(
            expected, content,
            ('Should allow a user to access '
                'a proposal from same store.')
        )

        url2 = reverse(
            'proposta_detalhes',
            args=[
                order2.id,
                proposal2.id
            ]
        )

        response2 = self.client.get(url2)
        try:
            actual = response2.url
        except AttributeError:
            actual = response2
        expected = '/not_authorized/'

        self.assertEqual(
            actual, expected,
            ('Should not allow a user to access '
                'a proposal from another store.')
        )

        url3 = reverse(
            'nova_proposta',
            args=[
                order2.id
            ]
        )

        response3 = self.client.get(url3)
        try:
            actual = response3.url
        except AttributeError:
            actual = response3
        expected = '/not_authorized/'

        self.assertEqual(
            actual, expected,
            ('Should not allow a user to create '
                'a new proposal for an order from '
                'another store.')
        )

        url4 = reverse(
            'alterar_proposta',
            args=[
                order2.id,
                proposal2.id
            ]
        )

        response4 = self.client.get(url4)
        try:
            actual = response4.url
        except AttributeError:
            actual = response4
        expected = '/not_authorized/'

        self.assertEqual(
            actual, expected,
            ('Should not allow a user to alter '
                'a proposal from another store.')
        )

        url5 = reverse(
            'proposta_contrato',
            args=[
                order2.id,
                proposal2.id
            ]
        )

        response5 = self.client.get(url5)
        try:
            actual = response5.url
        except AttributeError:
            actual = response5
        expected = '/not_authorized/'

        self.assertEqual(
            actual, expected,
            ('Should not allow a user to access '
                'contract from a proposal from another store.')
        )

        url6 = reverse(
            'proposta_dados_adicionais',
            args=[
                order2.id,
                proposal2.id
            ]
        )

        response6 = self.client.get(url6)
        try:
            actual = response6.url
        except AttributeError:
            actual = response6
        expected = '/not_authorized/'

        self.assertEqual(
            actual, expected,
            ('Should not allow a user to access '
                'additional info from a proposal '
                'from another store.')
        )

        url7 = reverse(
            'proposta_pagamento',
            args=[
                order2.id,
                proposal2.id
            ]
        )

        response7 = self.client.get(url7)
        try:
            actual = response7.url
        except AttributeError:
            actual = response7
        expected = '/not_authorized/'

        actual = response7.url
        expected = '/not_authorized/'

        self.assertEqual(
            actual, expected,
            ('Should not allow a user to access '
                'payment info from a proposal '
                'from another store.')
        )

        # url8 = reverse(
        #     'contrato_imprimir',
        #     args=[
        #         proposal2.id
        #     ]
        # )

        # response8 = self.client.get(url8)

        # actual = response8.status_code
        # expected = 405

        # self.assertEqual(
        #     actual, expected,
        #     ('Should not allow access to '
        #         'contrato_imprimir unless from POST')
        # )

        # url9 = reverse(
        #     'contrato_imprimir',
        #     args=[
        #         proposal2.id
        #     ]
        # )

        # response9 = self.client.post(url9)

        # actual = response9.status_code
        # expected = 200

        # self.assertEqual(
        #     actual, expected,
        #     ('Should not allow access to '
        #         'contrato_imprimir unless from POST')
        # )

    def test_access_service(self):
        '''Should test access of service page.'''
        self.test_login()

        service = self.service
        service2 = self.service2

        url = reverse(
            'editar_servico',
            args=[
                service.id,
            ]
        )

        response = self.client.get(url)

        content = str(response.content)
        expected = str('editar servi')

        self.assertIn(
            expected, content,
            ('Should allow a user to access '
                'a service from same store.')
        )

        url2 = reverse(
            'editar_servico',
            args=[
                service2.id,
            ]
        )

        response2 = self.client.get(url2)
        try:
            actual = response2.url
        except AttributeError:
            actual = response2
        expected = '/not_authorized/'

        self.assertEqual(
            actual, expected,
            ('Should not allow a user to access '
                'a service from another store.')
        )

        url3 = reverse(
            'editar_servico',
            args=[
                service.id,
            ]
        )

        person = self.person
        person.admin = False
        person.save()

        response3 = self.client.get(url3)
        try:
            actual = response3.url
        except AttributeError:
            actual = response3
        expected = '/not_authorized/'

        self.assertEqual(
            actual, expected,
            ('Should not allow a non admin user '
                'to access page editar_servico.')
        )

        person.admin = True
        person.save()


if __name__ == '__main__':
    unittest.main(verbosity=2)
