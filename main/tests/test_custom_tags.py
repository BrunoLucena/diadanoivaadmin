from django.test import TestCase

from main.templatetags.custom_tags import *


class CustomTagsCase(TestCase):
    '''Custom Tags'''

    def test_addstr(self):
        '''Should concatenate two strings.'''
        str1 = 'string_1'
        str2 = 'string_2'

        actual = addstr(str1, str2)
        expected = 'string_1string_2'

        self.assertEqual(
            actual, expected,
            ('Given two strings, it '
                'should return a new string with both strings concatenated.')
        )

    def test_Angularify(self):
        '''Should mark a string as safe.'''
        value = 'test'

        actual = Angularify(value)
        expected = str('{{test}}')

        self.assertEqual(
            actual, expected,
            ('Given a string, it '
                'should return a new string marked as safe.')
        )

    def test_percentage_minus(self):
        '''Should compute percentage from a given value and subtract from value.'''

        value = 100
        p = 10

        actual = percentage_minus(value, p)
        expected = 90

        self.assertEqual(
            actual, expected,
            ('Given a value and a percentage, it '
                'should compute the percentage and subtract from value.')
        )

    def test_percentage_plus(self):
        '''Should compute percentage from a given value and sum to value.'''

        value = 100
        p = 10

        actual = percentage_plus(value, p)
        expected = 110

        self.assertEqual(
            actual, expected,
            ('Given a value and a percentage, it '
                'should compute the percentage and sum to value.')
        )

    def test_ponto_para_virgula(self):
        '''Should replace dots to commas and return as string.'''

        value = 10.55

        actual = ponto_para_virgula(value)
        expected = '10,55'

        self.assertEqual(
            actual, expected,
            ('Given a number or string, it '
                'should replace dots to commas and return it as string.')
        )

    def teste_virgula_para_ponto(self):
        '''Should replace commas to dots and return as string.'''

        value = '10,55'

        actual = virgula_para_ponto(value)
        expected = '10.55'

        self.assertEqual(
            actual, expected,
            ('Given a string, it '
                'should replace commas to dots and return it as string.')
        )

    def test_multiply(self):
        '''Should multiply a number for given times.'''

        value = 49
        times = 5

        actual = multiply(value, times)
        expected = 245

        self.assertEqual(
            actual, expected,
            ('Given two numbers, it '
                'should multiply them.')
        )

    def test_sum(self):
        '''Should sum two numbers.'''

        a = 6
        b = 5

        actual = sum(a, b)
        expected = 11

        self.assertEqual(
            actual, expected,
            ('Given two numbers, it '
                'should sum them.')
        )
