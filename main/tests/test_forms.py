from django.test import TestCase

from main.tests.test_models import UserCase, StoreCase, PersonCase
from main.forms import *


class FormsCase(TestCase):
    '''Forms'''

    def setUp(self):
        self.user = UserCase.get_user(self)
        self.store = StoreCase.get_store(self)
        self.person = PersonCase.get_person(self)

    def test_user_form(self):
        '''Should return a valid form'''
        data = {
            'username': 'leticia',
            'first_name': 'Letícia',
            'last_name': 'Lucena',
            'email': 'le.silva89@gmail.com',
            'is_superuser': True
        }
        data2 = {
            'username': 'leticia',
            'first_name': 'Letícia',
            'last_name': 'Lucena',
            'email': 'le.silva89@gmail.com'
        }

        form = UserModelForm(data=data)
        form2 = UserModelForm2(data=data2)
        self.assertTrue(form.is_valid())
        self.assertTrue(form2.is_valid())
