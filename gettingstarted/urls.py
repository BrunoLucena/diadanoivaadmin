from django.conf import settings
from django.conf.urls import include, url
from django.contrib import admin
from django.contrib.auth.views import *  # login, logout

from main import views
from main.forms import AuthenticationFormCustom

admin.autodiscover()

handler400 = 'main.views.page_not_found'
handler403 = 'main.views.page_not_found'
handler404 = 'main.views.page_not_found'
handler500 = 'main.views.server_error'

urlpatterns = [
    url(
        r'^$', views.home, name='home'
    ),
    url(
        r'^accounts/profile/$',
        views.accounts_profile,
        name='accounts-profile'
    ),
    url(
        r'^web-admin/',
        admin.site.urls,
        name='admin'
    ),
    url(
        r'^cadastro_rapido/$',
        views.cadastro_rapido,
        name='cadastro_rapido'
    ),
    url(
        r'^cliente/(?P<order_id>\w+)/$',
        views.cliente,
        name='cliente'
    ),
    url(
        r'^cliente/(?P<order_id>\w+)/nova_proposta/$',
        views.nova_proposta,
        name='nova_proposta'
    ),
    url(
        r'^cliente/(?P<order_id>\w+)/proposta/(?P<proposalId>\w+)/$',
        views.proposta_detalhes,
        name='proposta_detalhes'
    ),
    url(
        r'^cliente/(?P<order_id>\w+)/proposta/(?P<proposalId>\w+)/alterar/$',
        views.alterar_proposta,
        name='alterar_proposta'
    ),
    url(
        r'^cliente/(?P<order_id>\w+)/proposta/(?P<proposalId>\w+)/contrato/$',
        views.proposta_contrato,
        name='proposta_contrato'
    ),
    url(
        r'^cliente/(?P<order_id>\w+)/proposta/(?P<proposalId>\w+)/dados_adicionais/$',
        views.proposta_dados_adicionais,
        name='proposta_dados_adicionais'
    ),
    url(
        r'^cliente/(?P<order_id>\w+)/proposta/(?P<proposalId>\w+)/pagamento/$',
        views.proposta_pagamento,
        name='proposta_pagamento'
    ),
    url(
        r'configuracoes/$',
        views.configuracoes,
        name='configuracoes'
    ),
    url(
        r'^cliente/(?P<order_id>\w+)/contrato/(?P<proposalId>\w+)/imprimir/$',
        views.contrato_imprimir,
        name='contrato_imprimir'
    ),
    url(
        r'^contrato/(?P<proposalId>\w+)/gerar_pdf/$',
        views.contrato_gerar_pdf,
        name='contrato_gerar_pdf'
    ),
    url(
        r'^login/$',
        login,
        {
            'authentication_form': AuthenticationFormCustom,
            'template_name': 'main/login.html',
            'redirect_field_name': '/'
        },
        name='login'
    ),
    url(
        r'^logout/$',
        views.logout_custom,
        name='logout'
    ),
    url(
        r'^not_authorized/$',
        views.not_authorized,
        name='not_authorized'
    ),
    url(
        r'^perfil/$',
        views.perfil,
        name='perfil'
    ),
    url(
        r'^propostas/$',
        views.propostas,
        name='propostas'
    ),
    url(
        r'^cliente/(?P<order_id>\w+)/proposta/(?P<proposal_id>\w+)/imprimir/$',
        views.proposta_imprimir,
        name='proposta_imprimir'
    ),
    url(
        r'^proposta/(?P<proposal_id>\w+)/gerar_pdf/(?P<token_access>\w+)/$',
        views.proposta_gerar_pdf,
        name='proposta_gerar_pdf'
    ),
    url(
        r'^servicos/$',
        views.servicos,
        name='servicos'
    ),
    url(
        r'^servicos/criar/$',
        views.criar_servico,
        name='criar_servico'
    ),
    url(
        r'^servicos/(?P<service_id>\w+)/$',
        views.editar_servico,
        name='editar_servico'
    ),
    url(
        r'^teste/$',
        views.teste,
        name='teste'
    ),
    url(
        r'^usuarios/$',
        views.usuarios,
        name='usuarios'
    ),
    url(
        r'^usuarios/criar/$',
        views.criar_usuario,
        name='criar_usuario'
    ),
    url(
        r'^usuarios/(?P<user_id>\w+)/$',
        views.editar_usuario,
        name='editar_usuario'
    ),
]

if 'rosetta' in settings.INSTALLED_APPS:
    urlpatterns += [
        url(r'^rosetta/', include('rosetta.urls'))
    ]

# urlpatterns += patterns('',
#         (r'^static/(?P<path>.*)$','django.views.static.serve', {'document_root': settings.STATIC_ROOT}),
#     )
