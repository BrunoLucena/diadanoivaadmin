function showBox(whichBox) {
	var box = $(whichBox);
	var boxContent = box.find(".box-content");
	var plus = box.find(".fa-plus-box");
	var minus = box.find(".fa-minus-box");

	plus.hide();
	minus.show();
	boxContent.show(250);
}

function hideBox(whichBox) {
	var box = $(whichBox);
	var boxContent = box.find(".box-content");
	var plus = box.find(".fa-plus-box");
	var minus = box.find(".fa-minus-box");

	plus.show();
	minus.hide();
	boxContent.hide(250);
}

function addServicePrice() {
	var div = $(".service_prices");
	var tbody = $(".service_prices .table tbody ");
	var numPrices = $(".number_of_prices");
	var numPricesCount = parseInt(numPrices.html());
	var html = tbody.html();

	numPricesCount += 1;
	html += '\
		<tr> \n\
			<td><input type="number" class="form-control service_price_' + numPricesCount + '" name="service_price_' + numPricesCount + '" value="0" size="6" min="0" step="0.01"></td> \n\
			<td><input type="text" class="form-control service_date_start_' + numPricesCount + ' datemask" name="service_date_start_' + numPricesCount + '"></td> \n\
			<td><input type="text" class="form-control service_date_end_' + numPricesCount + ' datemask" name="service_date_end_' + numPricesCount + '"> \n\
			<td><button type="button" class="btn btn-danger service_delete" name="service_delete"><i class="fa fa-close"></i></td> \n\
		</tr> \n';

	numPrices.html(numPricesCount);
	tbody.html(html);

	$(".datemask").inputmask("dd/mm/yyyy", {"placeholder": "dd/mm/yyyy" });
}

function initialServicePrices() {
	var div = $(".service_prices");
	var tbody = $(".service_prices .table tbody ");
	var numPrices = $(".number_of_prices");
	var numPricesCount = parseInt(numPrices.html());
	var html = tbody.html();

	for (i=0; i < numPricesCount; i++) {
		html += '\
		<tr> \n\
			<td><input type="number" class="form-control service_price_' + i + '" name="service_price_' + i + '" value="0" size="6" min="0" step="0.01"></td> \n\
			<td><input type="text" class="form-control service_date_start_' + i + ' datemask" name="service_date_start_' + i + '"></td> \n\
			<td><input type="text" class="form-control service_date_end_' + i + ' datemask" name="service_date_end_' + i + '"> \n\
			<td><button type="button" class="btn btn-danger service_delete" name="service_delete"><i class="fa fa-close"></i></td> \n\
		</tr> \n';
	}

	tbody.html(html);

	$(".datemask").inputmask("dd/mm/yyyy", {"placeholder": "dd/mm/aaaa" });
}

function deleteServicePrice(object) {
	var tr = object.parent().parent();
	tr.remove();
}

function calculateAllServices() {
	var allServices = $("input.service_checkbox:checkbox");
	var checkedServices = [];
	var uncheckedServices = [];
	var discountChecked = $(".discount_checkbox").is(":checked");
	var discountPercentage = $(".discount_percentage").val();
	var total = $("#total");
	var discount = $("#total_discount");
	var totalFixed = $("#total_fixed").text();
	var newTotal = 0;

	if (parseInt(discountPercentage) > 100
				| parseInt(discountPercentage) < 0
				| jQuery.type(discountPercentage) === "undefined"
				| jQuery.type(discountPercentage) === "null"
				| discountPercentage == "") {
		discountPercentage = "0";
	}

	var discountTotal = (parseFloat(totalFixed.replace(",", ".")) * parseInt(discountPercentage) / 100).toFixed(2);

	allServices.each(function(){
		if ($(this).is(":checked")) {
			var price = parseFloat($(this).siblings().val().replace(",", "."));
			newTotal = parseFloat(newTotal) + parseFloat(price);
			newTotal = newTotal.toFixed(2);
		}
	});

	if (discountChecked) {
		newTotal = newTotal - discountTotal;
		newTotal = newTotal.toFixed(2);
		discount.text(discountTotal.replace(".", ","));
	}

	total.text(String(newTotal).replace(".", ","));

}

function getInstallments() {
	var installmentsAll = $(".installments_all");
	var qtInstallments = parseFloat($(".installments_qt").val());
	var totalToPay = parseFloat($("#total_to_pay").val().replace(",", "."));
	var notFirstInstallment = Math.floor(totalToPay / qtInstallments);
	var remainder = totalToPay % qtInstallments;
	var firstInstallment = (notFirstInstallment + remainder).toFixed(2);
	var html = "";
	var today = moment();
	getMaxInstallments()

	for (i = 1; i <= qtInstallments; i++) {
		var newDate = today.add(30, "days");
		var newDateStr = newDate.format("DD/MM/YYYY");
		if (i == 1) {
			html += " \
				<div class='form-group row'> \
					<label class='control-label col-sm-3' for='installment" + i + "'>Parcela " + i + "</label> \
					<div class='col-sm-3'> \
						<input class='form-control' type='number' value='" + firstInstallment + "' name='installment" + i + "' min='1' \
										max='" + totalToPay + "' step='0.01'> \
					</div> \
					<div class='col-sm-4'> \
						<input type='text' class='form-control datemask' name='installment" + i + "date' id='installment' " + i + "' value='" + newDateStr + "'> \
					</div> \
				</div>";
		} else {
			html += " \
				<div class='form-group row'> \
					<label class='control-label col-sm-3' for='installment" + i + "'>Parcela " + i + "</label> \
					<div class='col-sm-3'> \
						<input class='form-control' type='number' value='" + notFirstInstallment + "' name='installment" + i + "' min='1' \
										max='" + totalToPay + "' step='0.01'> \
					</div> \
					<div class='col-sm-4'> \
						<input type='text' class='form-control datemask' name='installment" + i + "date' id='installment' " + i + "' value='" + newDateStr + "'> \
					</div> \
				</div>";
		}
	}

	installmentsAll.html(html);

	$(".datemask").inputmask("dd/mm/yyyy", {"placeholder": "dd/mm/aaaa" });
}

function getPaymentType() {
	var installmentsQt = $(".installments_qt");
	var maxInstallments = jQuery.parseJSON($(this).val());

	$.each(maxInstallments, function(key, value) {
		installmentsQt.attr({"max": value});
		if (parseInt(installmentsQt.val()) > parseInt(value)) {
			installmentsQt.val(installmentsQt.attr("max"));
			getInstallments();
		}
	});

	getMaxInstallments()
}

function getMaxInstallments() {
	var installmentsQt = $(".installments_qt");
	var maxInstallments = jQuery.parseJSON($(".payment_type").val());
	var keys = Object.keys(maxInstallments);
	var key = keys[0];
	var maxInstallmentsNumber = maxInstallments[key];
	var maxInstallmentsEl = $(".max_installments");
	maxInstallmentsEl.html(maxInstallmentsNumber);

	return maxInstallmentsNumber;
}

function main() {
	$(".fa-plus-box").click(function() {
			var box = $(this).closest(".box");
			showBox(box);
	});

	$(".fa-minus-box").click(function() {
			var box = $(this).closest(".box");
			hideBox(box);
	});

	$(".service_checkbox, .discount_checkbox, .discount_percentage").change(calculateAllServices);

	$(".payment_type").change(getPaymentType);

	$(".installments_qt").change(getInstallments);

	$(".service_add_price").click(addServicePrice);

	$(".service_prices").on("click", ".service_delete", function(){
		deleteServicePrice($(this));
		$(".datemask").inputmask("dd/mm/yyyy", {"placeholder": "dd/mm/yyyy" });
	});
};


$(document).ready(main);